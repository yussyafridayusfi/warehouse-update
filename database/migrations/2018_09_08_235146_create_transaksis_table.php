<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomortransaksi');
            $table->string('jenistransaksi');
            $table->unsignedInteger('id_barang');
            $table->unsignedInteger('id_customer')->nullable()->default(null);
            $table->unsignedInteger('id_suplier')->nullable()->default(null);
            $table->unsignedInteger('id_user');
            $table->integer('jumlah');
            $table->integer('harga');
            $table->integer('total');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('id_customer')->references('id')->on('customers');
            $table->foreign('id_barang')->references('id')->on('barangs');
            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_suplier')->references('id')->on('supliers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksis');
    }
}
