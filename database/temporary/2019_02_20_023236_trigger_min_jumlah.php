<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TriggerMinJumlah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::getPdo()->exec
        ('
            CREATE TRIGGER min_jumlah_barang AFTER INSERT ON `transaksis` FOR EACH ROW
            BEGIN
                IF (NEW.jumlah <= `jumlah`)
                THEN
                UPDATE `barangs` SET `jumlah`= jumlah + NEW.jumlah
                WHERE `id_barang`=NEW.id_barang;
                END IF;
            END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // DB::unprepared('DROP TRIGGER `min_jumlah_barang`');
    }
}
