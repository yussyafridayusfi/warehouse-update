<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TriggerAddJumlah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::getPdo()->exec
        ('
            CREATE TRIGGER add_jumlah_barang AFTER UPDATE ON `barangs` FOR EACH ROW
            BEGIN
                UPDATE `barangs` SET `jumlah`= OLD.jumlah + NEW.jumlah
                WHERE `id`=NEW.id;
            END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         DB::unprepared('DROP TRIGGER `add_jumlah_barang`');
    }
}
