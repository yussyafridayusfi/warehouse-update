<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class barang extends Model
{
    protected $fillable = [
        'nama_barang', 'kode_barang', 'id_kategori', 'jumlah', 'harga', 'foto_barang','deskripsi',
    ]; 
  
    public function kategori()
    {
        return $this->belongsTo(kategori::class);   
    }

     public function logs()
    {
        return $this->hasMany(logs::class);
    }


    use SoftDeletes;
    protected $dates =['deleted_at'];
}
