<?php

namespace App;


use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\User
 *
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama','alamat','email','password','telp','foto_profil','jabatan',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function barang()
    {
        return $this->hasMany(barang::class);
    }

    public function transaksi()
    {
        return $this->hasMany(transaksi::class);
    }   

    public function hasRole($role)
    {
        return $this->role == $role;
    } 

    public function hasId($id)
    {
        return $this->id == $id;
    }
   
}
    
