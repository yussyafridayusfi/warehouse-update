<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class retur extends Model
{
    protected $fillable = [
        'jenisretur','id_transaksi','id_barang', 'jumlah', 'keterangan', 'nama_target',
    ];

    public function nomortr()
    {
        return $this->belongsTo(nomortr::class);   
    }
    public function barang()
    {
        return $this->belongsTo(barang::class);   
    }

    
}
