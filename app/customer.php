<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class customer extends Model
{

    protected $fillable = [
        'nama', 'alamat', 'telp',
    ];

    public function transaksi()
    {
        return $this->hasMany(transaksi::class);
    }

    use SoftDeletes;
    protected $dates =['deleted_at'];
}
