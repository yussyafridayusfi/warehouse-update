<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class transaksi extends Model
{
    protected $fillable = [
        'jenistransaksi','nomortransaksi','id_customer', 'id_barang', 'id_user','id_suplier', 'jumlah', 'harga','total',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);   
    }   
    public function customer()
    {
        return $this->belongsTo(customer::class); 
    }
    public function barang()
    {
        return $this->belongsTo(barang::class);   
    }
      public function suplier()
    {
        return $this->belongsTo(suplier::class); 
    }

     //use SoftDeletes;
     //protected $dates =['deleted_at'];
}
