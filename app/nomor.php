<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class nomor extends Model
{
    protected $fillable = [
        'nomor',
    ];

    public function transaksi()
    {
        return $this->hasMany(transaksi::class);
    }

    public function retur()
    {
        return $this->hasMany(retur::class);
    }

    use SoftDeletes;
    protected $dates =['deleted_at'];
}
