<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class kategori extends Model
{
    protected $fillable = [
        'nama_kategori' ,
    ];

    public function barang()
    {
        return $this->hasMany(barang::class);
    }

    

    use SoftDeletes;
    protected $dates =['deleted_at'];
}
