<?php

namespace App\Http\Controllers;

use App\User;
use App\transaksi;
use App\customer;
use App\barang;
use App\suplier;
use App\retur;
use App\Http\Controllers\DB;
use App\Http\Controllers\View;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\userController;
use App\Http\Controllers\customerController;
use App\Http\Controllers\barangController;
use App\Http\Controllers\returController;
use App\Http\Controllers\supplierController;
//use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection;

class dashboardController extends Controller
{
    public function index()
    {
    	//hitung customer
    	$countCustomer = customer::count();

    	//hitung supplier
    	$countSupplier = suplier::count();

    	//hitung jumlah laptop
    	$laptops = \DB::table('barangs')
    	->where ('barangs.id_kategori','1')
        ->where ('barangs.deleted_at', null);
    	$laptop = $laptops->sum('jumlah');

    	//hitung jumlah laptop-gaming
    	$gamings = \DB::table('barangs')
    	->where ('barangs.id_kategori','2')
        ->where('barangs.deleted_at', null);
    	$gaming = $gamings->sum('jumlah');

    	//hitung jumlah notebook
    	$notebooks = \DB::table('barangs')
    	->where ('barangs.id_kategori','3')
        ->where('barangs.deleted_at', null);
    	$notebook = $notebooks->sum('jumlah');

    	//hitung jumlah netbook
    	$netbooks = \DB::table('barangs')
    	->where ('barangs.id_kategori','4')
        ->where('barangs.deleted_at', null);
    	$netbook = $netbooks->sum('jumlah');

    	//hitung jumlah macbook
    	$macbooks = \DB::table('barangs')
    	->where ('barangs.id_kategori','5')
        ->where('barangs.deleted_at', null);
    	$macbook = $macbooks->sum('jumlah');

    	//hitung jumlah total barang
    	$sumBarang = \DB::table('barangs')
        ->where('barangs.deleted_at', null)
        ->sum('jumlah');

    	//harga total transaksi tertinggi
    	$maxTrans = \DB::table('transaksis')->max('total');



        return view ('dashboard')
        ->with (compact('countCustomer'))
        ->with (compact('countSupplier'))
        ->with (compact('laptop'))
        ->with (compact('gaming'))
        ->with (compact('notebook'))
        ->with (compact('netbook'))
        ->with (compact('macbook'))
        ->with (compact('sumBarang'))
        ->with (compact('maxTrans'));
    }

    public function getCountCustomer()
    {
    	$countCustomer = barang::count();
    	 return view ('dashboard')
    	 ->with (compact('barangs'));
    }

    public function getSumItem()
    {
    	//fix sum
    	$barangs = \DB::table('barangs')->sum('jumlah');


    }

    public function getSumLaptop()
    {
    	$barangs = \DB::table('barangs')
    	->where ('barangs.id_kategori','1');
    	$barang = $barangs->sum('jumlah');

    }

    public function getMaxMin()
    {
    	$barangs = \DB::table('barangs')->max('jumlah');

    	$barangs = \DB::table('barangs')->min('jumlah');
    }

    public function getLaporanMember()
    {

        $suppliers = suplier::all();
        //$customers = customer::all();

        return view ('laporan.createlaporanmember')
        ->with(compact('suppliers'));
        //->with(compact('customers'));
    }

    public function getDataMember(Request $request)
    {
        //dd($request->reservation);
        $dates = $request->reservation;
        //$dates=date('Y/m/d');

        //$dates = explode(' - ', $dates);
        //$startDate = $dates[0];
        //$endDate = $dates[1];

        //get array [$,$df] if only need array[1]
        [$startDate,$endDate] = explode(' - ', $dates);

        //dd($startDate);

        $startCarbonDate = Carbon::createFromFormat('m/d/Y', $startDate);
        $start = $startCarbonDate->format('Y-m-d');

        $endCarbonDate = Carbon::createFromFormat('m/d/Y', $endDate);
        $end = $endCarbonDate->format('Y-m-d');

        $suppliers = suplier::whereBetween('created_at',[$start,$end])->get();
        //$customers = customer::whereBetween('created_at',[$start,$end])->get();
        //dd($suplier);
        //$customer = customer::whereBetween('created_at',[$startDate,$endDate])->get();
        //return response()->json($suplier);
        return view ('laporan.createlaporanmember')
        ->with(compact('suppliers'));


        //return response()->json($customer);
    }


    public function getLaporanBarang(Request $request)
    {
        $barangs = \DB::table('barangs')          
        -> join('kategoris', 'barangs.id_kategori', '=', 'kategoris.id')
        -> select('barangs.id','barangs.nama_barang', 'barangs.kode_barang','kategoris.nama_kategori','barangs.jumlah','barangs.harga','barangs.foto_barang','barangs.deskripsi','barangs.created_at','barangs.updated_at')            
        //->where('barangs.deleted_at', null)
        ->get();
        return view ('laporan.createlaporanbarang',compact('barangs'));
    }

    public function getDataBarang(Request $request)
    {
        $dates = $request->reservation;
        [$startDate,$endDate] = explode(' - ', $dates);

        $startCarbonDate = Carbon::createFromFormat('m/d/Y', $startDate);
        $start = $startCarbonDate->format('Y-m-d');

        $endCarbonDate = Carbon::createFromFormat('m/d/Y', $endDate);
        $end = $endCarbonDate->format('Y-m-d');

        //$barangs = barang::whereBetween('created_at',[$start,$end])->get();

        $barangs = \DB::table('barangs')          
        -> join('kategoris', 'barangs.id_kategori', '=', 'kategoris.id')
        -> select('barangs.id','barangs.nama_barang', 'barangs.kode_barang','kategoris.nama_kategori','barangs.jumlah','barangs.harga','barangs.foto_barang','barangs.deskripsi','barangs.created_at','barangs.updated_at')            
        ->whereBetween('barangs.created_at',[$start,$end])
        ->get();

        //return response()->json($barangs);
        return view ('laporan.createlaporanbarang')
        ->with(compact('barangs'));
    }



    public function getLaporanTrans()
    {
        $transaksis = \DB::table('transaksis')
        -> join('barangs', 'transaksis.id_barang', '=', 'barangs.id')
        -> join('users', 'transaksis.id_user', '=', 'users.id')
        -> select('transaksis.id','transaksis.nomortransaksi','transaksis.jenistransaksi as jenis','barangs.nama_barang as nama_barang','users.nama as nama_user','transaksis.jumlah','transaksis.harga','transaksis.total','transaksis.created_at','transaksis.updated_at')            
        // ->where('barangs.id', 1) 
        ->get();
        return view ('laporan.createlaporantrans',compact('transaksis')); 
    }

    public function getDataTrans(Request $request)
    {
        $dates = $request->reservation;
        [$startDate,$endDate] = explode(' - ', $dates);

        $startCarbonDate = Carbon::createFromFormat('m/d/Y', $startDate);
        $start = $startCarbonDate->format('Y-m-d');

        $endCarbonDate = Carbon::createFromFormat('m/d/Y', $endDate);
        $end = $endCarbonDate->format('Y-m-d');

        //$transaksis = transaksi::whereBetween('created_at',[$start,$end])->get();
        $transaksis = \DB::table('transaksis')
        -> join('barangs', 'transaksis.id_barang', '=', 'barangs.id')
        -> join('users', 'transaksis.id_user', '=', 'users.id')
        -> select('transaksis.id','transaksis.nomortransaksi','transaksis.jenistransaksi as jenis','barangs.nama_barang as nama_barang','users.nama as nama_user','transaksis.jumlah','transaksis.harga','transaksis.total','transaksis.created_at','transaksis.updated_at')            
        ->whereBetween('transaksis.created_at',[$start,$end]) 
        ->get();

        //return response()->json($transaksis);
        return view ('laporan.createlaporantrans')
        ->with(compact('transaksis'));
    }

    public function getLaporanRetur()
    {
        $returs = \DB::table('returs')          
        -> join('transaksis', 'returs.id_transaksi', '=', 'transaksis.id')
        -> join('barangs', 'returs.id_barang', '=', 'barangs.id')
        -> select('returs.id','transaksis.nomortransaksi','returs.jenisretur','barangs.nama_barang as nama_barang','returs.jumlah','returs.keterangan','returs.created_at','returs.updated_at')            
        // ->where('barangs.id', 1) 
        ->get();
        return view ('laporan.createlaporanretur',compact('returs')); 
    }

    public function getDataRetur(Request $request)
    {
        $dates = $request->reservation;
        [$startDate,$endDate] = explode(' - ', $dates);

        $startCarbonDate = Carbon::createFromFormat('m/d/Y', $startDate);
        $start = $startCarbonDate->format('Y-m-d');

        $endCarbonDate = Carbon::createFromFormat('m/d/Y', $endDate);
        $end = $endCarbonDate->format('Y-m-d');

        //$returs = retur::whereBetween('created_at',[$start,$end])
        $returs = \DB::table('returs')
        -> join('transaksis', 'returs.id_transaksi', '=', 'transaksis.id')
        -> join('barangs', 'returs.id_barang', '=', 'barangs.id')
        -> select('returs.id','transaksis.nomortransaksi','returs.jenisretur','barangs.nama_barang as nama_barang','returs.jumlah','returs.keterangan','returs.created_at','returs.updated_at')   
        ->whereBetween('returs.created_at',[$start,$end])
        ->get();
 
        /*->where(function($query) use ($start,$end){
            $query->whereBetween('created_at',array($start,$end));
        })*/
        
        
        //return $returs->whereBetween('created_at',[$start,$end]);


        //return response()->json($returs);
        return view ('laporan.createlaporanretur')
        ->with(compact('returs'));
    }

    public function getLogs()
    {
        $suppliers = suplier::all();
        $customers = customer::all();
        return view ('laporan.masterlogs')
        ->with(compact('suppliers'))
        ->with(compact('customers'));
    }

}
