<?php

namespace App\Http\Controllers;

use App\customer;
use Illuminate\Http\Request;
use App\Http\Requests;

class customerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = customer::all();
        return view ('customer.mastercustomer',compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('customer.createcustomer');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = 'failed';
        $customer = customer::create([
            'nama'=> $request->nama,
            'alamat'=> $request->alamat,
            'telp'=> $request->telp
        ]); {
           $success = 'success'; 
        }
        


        // Customer::create($request->all());

        return redirect()->route('customer.index')->with('alert', $success);
        // return view ('mastercustomer')->with('alert', $success);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $customers = customer::find($id);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customers = customer::find($id);
        $customer = $customers;
        return view('customer.editcustomer',compact('customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        customer::where('id', $id)->update([
            'nama' => request('nama'),
            'alamat' => request('alamat'),
            'telp' => request('telp')
        ]);

        return redirect()->route('customer.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customers = customer::findOrFail($id);
        $customers->delete();

        return redirect()->route('customer.index');
    }

}
