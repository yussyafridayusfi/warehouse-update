<?php

namespace App\Http\Controllers;

use App\logs;
use App\Customer;
use App\Barang;
use App\User;
use App\Suplier;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\DB;
use App\Http\Controllers\View;
use App\Http\Controllers\barangController;
use App\Http\Controllers\customerController;
use App\Http\Controllers\supplierController;

class LogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\logs  $logs
     * @return \Illuminate\Http\Response
     */
    public function show(logs $logs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\logs  $logs
     * @return \Illuminate\Http\Response
     */
    public function edit(logs $logs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\logs  $logs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, logs $logs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\logs  $logs
     * @return \Illuminate\Http\Response
     */
    public function destroy(logs $logs)
    {
        //
    }

    public function getLapKeluar()
    {
        $logs = \DB::table('logs')          
        -> join('barangs', 'logs.id_barang', '=', 'barangs.id')
        -> join('customers', 'logs.nama_target', '=', 'customers.id')
        -> select('logs.id','logs.jenis_logs','logs.nama_logs','barangs.nama_barang as nama_barang','logs.user','customers.nama as nama_target','logs.keterangan','logs.stok_lama','logs.stok_baru','logs.created_at')            
        ->where('logs.jenis_logs', 'transaksi')
        ->where('logs.nama_logs', 'barang-keluar')
        ->get();
        return view ('laporan.masterlogs-keluar',compact('logs')); 
    }

    public function getDataLapKeluar(Request $request)
    {
        $dates = $request->reservation;
        [$startDate,$endDate] = explode(' - ', $dates);

        $startCarbonDate = Carbon::createFromFormat('m/d/Y', $startDate);
        $start = $startCarbonDate->format('Y-m-d');

        $endCarbonDate = Carbon::createFromFormat('m/d/Y', $endDate);
        $end = $endCarbonDate->format('Y-m-d');

        //$transaksis = transaksi::whereBetween('created_at',[$start,$end])->get();
        $logs = \DB::table('logs')          
        -> join('barangs', 'logs.id_barang', '=', 'barangs.id')
        -> join('customers', 'logs.nama_target', '=', 'customers.id')
        -> select('logs.id','logs.jenis_logs','logs.nama_logs','barangs.nama_barang as nama_barang','logs.user','customers.nama as nama_target','logs.keterangan','logs.stok_lama','logs.stok_baru','logs.created_at')            
        ->where('logs.jenis_logs', 'transaksi')
        ->where('logs.nama_logs', 'barang-keluar')           
        ->whereBetween('logs.created_at',[$start,$end]) 
        ->get();

        //return response()->json($transaksis);
        return view ('laporan.masterlogs-keluar',compact('logs')); 
    }

    public function getLapMasuk()
    {
        $logs = \DB::table('logs')          
        -> join('barangs', 'logs.id_barang', '=', 'barangs.id')
        -> join('supliers', 'logs.nama_target', '=', 'supliers.id')
        -> select('logs.id','logs.jenis_logs','logs.nama_logs','barangs.nama_barang as nama_barang','logs.user','supliers.nama as nama_target','logs.keterangan','logs.stok_lama','logs.stok_baru','logs.created_at')            
        ->where('logs.jenis_logs', 'transaksi')
        ->where('logs.nama_logs', 'barang-masuk')
        ->get();
        return view ('laporan.masterlogs-masuk',compact('logs')); 
    }

    public function getDataLapMasuk(Request $request)
    {
        $dates = $request->reservation;
        [$startDate,$endDate] = explode(' - ', $dates);

        $startCarbonDate = Carbon::createFromFormat('m/d/Y', $startDate);
        $start = $startCarbonDate->format('Y-m-d');

        $endCarbonDate = Carbon::createFromFormat('m/d/Y', $endDate);
        $end = $endCarbonDate->format('Y-m-d');

        //$transaksis = transaksi::whereBetween('created_at',[$start,$end])->get();
        $logs = \DB::table('logs')          
        -> join('barangs', 'logs.id_barang', '=', 'barangs.id')
        -> join('supliers', 'logs.nama_target', '=', 'supliers.id')
        -> select('logs.id','logs.jenis_logs','logs.nama_logs','barangs.nama_barang as nama_barang','logs.user','supliers.nama as nama_target','logs.keterangan','logs.stok_lama','logs.stok_baru','logs.created_at')            
        ->where('logs.jenis_logs', 'transaksi')
        ->where('logs.nama_logs', 'barang-masuk')         
        ->whereBetween('logs.created_at',[$start,$end]) 
        ->get();

        //return response()->json($transaksis);
        return view ('laporan.masterlogs-keluar',compact('logs')); 
    }

    public function getLapRKeluar()
    {
        $logs = \DB::table('logs')          
        -> join('barangs', 'logs.id_barang', '=', 'barangs.id')
        -> join('customers', 'logs.nama_target', '=', 'customers.id')
        -> select('logs.id','logs.jenis_logs','logs.nama_logs','barangs.nama_barang as nama_barang','logs.user','customers.nama as nama_target','logs.keterangan','logs.stok_lama','logs.stok_baru','logs.created_at')            
        ->where('logs.jenis_logs', 'retur')
        ->where('logs.nama_logs', 'barang-keluar')  
        ->get();
        return view ('laporan.masterlogs-returkeluar',compact('logs')); 
    }

    public function getDataLapRKeluar(Request $request)
    {
        $dates = $request->reservation;
        [$startDate,$endDate] = explode(' - ', $dates);

        $startCarbonDate = Carbon::createFromFormat('m/d/Y', $startDate);
        $start = $startCarbonDate->format('Y-m-d');

        $endCarbonDate = Carbon::createFromFormat('m/d/Y', $endDate);
        $end = $endCarbonDate->format('Y-m-d');

        //$transaksis = transaksi::whereBetween('created_at',[$start,$end])->get();
        $logs = \DB::table('logs')          
        -> join('barangs', 'logs.id_barang', '=', 'barangs.id')
        -> join('customers', 'logs.nama_target', '=', 'customers.id')
        -> select('logs.id','logs.jenis_logs','logs.nama_logs','barangs.nama_barang as nama_barang','logs.user','customers.nama as nama_target','logs.keterangan','logs.stok_lama','logs.stok_baru','logs.created_at')            
        ->where('logs.jenis_logs', 'retur')
        ->where('logs.nama_logs', 'barang-keluar')         
        ->whereBetween('logs.created_at',[$start,$end]) 
        ->get();

        //return response()->json($transaksis);
        return view ('laporan.masterlogs-keluar',compact('logs')); 
    }

    public function getLapRMasuk()
    {
        $logs = \DB::table('logs')          
        -> join('barangs', 'logs.id_barang', '=', 'barangs.id')
        -> join('supliers', 'logs.nama_target', '=', 'supliers.id')
        -> select('logs.id','logs.jenis_logs','logs.nama_logs','barangs.nama_barang as nama_barang','logs.user','supliers.nama as nama_target','logs.keterangan','logs.stok_lama','logs.stok_baru','logs.created_at')            
        ->where('logs.jenis_logs', 'retur')
        ->where('logs.nama_logs', 'barang-masuk')
        ->get();
        return view ('laporan.masterlogs-returmasuk',compact('logs')); 
    }

    public function getDataLapRMasuk(Request $request)
    {
        $dates = $request->reservation;
        [$startDate,$endDate] = explode(' - ', $dates);

        $startCarbonDate = Carbon::createFromFormat('m/d/Y', $startDate);
        $start = $startCarbonDate->format('Y-m-d');

        $endCarbonDate = Carbon::createFromFormat('m/d/Y', $endDate);
        $end = $endCarbonDate->format('Y-m-d');

        //$transaksis = transaksi::whereBetween('created_at',[$start,$end])->get();
        $logs = \DB::table('logs')          
        -> join('barangs', 'logs.id_barang', '=', 'barangs.id')
        -> join('supliers', 'logs.nama_target', '=', 'supliers.id')
        -> select('logs.id','logs.jenis_logs','logs.nama_logs','barangs.nama_barang as nama_barang','logs.user','supliers.nama as nama_target','logs.keterangan','logs.stok_lama','logs.stok_baru','logs.created_at')            
        ->where('logs.jenis_logs', 'retur')
        ->where('logs.nama_logs', 'barang-masuk')        
        ->whereBetween('logs.created_at',[$start,$end]) 
        ->get();

        //return response()->json($transaksis);
        return view ('laporan.masterlogs-keluar',compact('logs')); 
    }

}
