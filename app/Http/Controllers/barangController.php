<?php

namespace App\Http\Controllers;

use App\barang;
use App\User;
use App\kategori;
use App\suplier;
use App\Http\Controllers\DB;
use App\Http\Controllers\View;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\userController;
use App\Http\Controllers\kategoriController;
use App\Http\Controllers\supplierController;
use Illuminate\Database\Query\Builder;

class barangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //table name
         $barangs = \DB::table('barangs')          
        -> join('kategoris', 'barangs.id_kategori', '=', 'kategoris.id')
       // -> join('users', 'barangs.id_user', '=', 'users.id')
        //-> join('supliers', 'barangs.id_suplier', '=', 'supliers.id')
        -> select('barangs.id','barangs.nama_barang', 'barangs.kode_barang','kategoris.nama_kategori','barangs.jumlah','barangs.harga','barangs.foto_barang','barangs.deskripsi','barangs.created_at','barangs.updated_at')            
        ->where('barangs.deleted_at', null) 
        ->get();
        return view ('barang.masterbarang',compact('barangs')); 
        
        // dd($barangs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $maxId = \DB::table('barangs')->max('id');
        $maxId= $maxId + 1;

        $users = User::all();
        $kategoris = kategori::all();
        $suppliers = suplier::all();

        return view ('barang.createbarang')
        ->with(compact('users'))
        ->with(compact('kategoris'))
        ->with(compact('suppliers'))
        ->with(compact('maxId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = 'failed';

        $barang = barang::create([
            'nama_barang'=> $request->nama_barang,
            'kode_barang'=> $request->kode_barang,
            'id_kategori'=> $request->id_kategori,
            'jumlah'=>$request->jumlah,
            'harga'=>$request->harga,
            'foto_barang'=> $request->file('foto_barang')->store('foto_barang'),
            'deskripsi'=>$request->deskripsi
        ]); 

        // $this->validate($request,[
        //     'kode_barang'=>'required|unique:barangs'
        // ]);

        $this->validate($request,[
            'foto_barang'=>'required|mimes:jpeg,jpg,png'
        ]);

        {
           $success = 'success'; 
        }
        //route('barang.index')
        return redirect()->back()->with('alert', $success);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kategoris = kategori::all();
        $suppliers = suplier::all();
        $users = user::all();
        $barangs = barang::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategoris = kategori::all();
        $suppliers = suplier::all();
        $users = user::all();
        $barangs = barang::find($id);
        
        return view('barang.editbarang',compact('kategoris', 'suppliers','users','barangs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        barang::where('id', $id)->update([
            'nama_barang' => request('nama_barang'),
            'kode_barang' => request('kode_barang'),
            'id_kategori' => request('id_kategori'),
            // 'id_suplier' => request('id_suplier'),
            // 'id_user' => request('id_user'),
            // 'jumlah' => (request('jumlah') + request('jml')),
            'jumlah' => request('jumlah'),
            'harga' => request('harga'),
            // 'foto_barang'=> $request->file('foto_barang')->store('foto_barang'),
            'deskripsi' => request('deskripsi')
        ]);

        return redirect()->route('barang.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $barangs = barang::findOrFail($id);
        $barangs->delete();

        //Session::flash('flash_message', 'Customer successfully deleted!');

        return redirect()->route('barang.index')
        ->with('delete','Anda telah berhasil menghapus produk');
        //return redirect()->route('/barang');
    }

}
