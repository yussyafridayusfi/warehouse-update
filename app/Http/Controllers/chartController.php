<?php

namespace App\Http\Controllers;

use App\Barang;
use App\User;
use App\Kategori;
use App\Suplier;
use App\Http\Controllers\DB;
use Illuminate\Http\Request;

class chartController extends Controller
{
    public function index()
    {
    	$result = \DB::table('barangs')
                    ->where('jumlah','>','0')
                    ->orderBy('id_kategori', 'ASC')
                    ->get();
        return response()->json($result);
    }
}
