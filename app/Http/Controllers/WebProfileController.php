<?php

namespace App\Http\Controllers;

use App\barang;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder;
use Illuminate\Pagination\Paginator;

class WebProfileController extends Controller
{
    public function getHome()
    {
    	// $newest = \DB::table('barangs')->orderBy('created_at', 'desc')->first();

    	//latest -> method orderBy desc from collection
    	$newwest = barang::latest();
    	$newest = $newwest->paginate(8);

		return view ('web-profile.webProfileHome')
        	->with(compact('newest'));
    }

    public function getLaptop()
    {
    	$kategori1 = \DB::table('barangs')
        ->select('barangs.nama_barang as nama_barang','barangs.id','barangs.harga','barangs.id_kategori','barangs.foto_barang','barangs.jumlah')
       	->where('barangs.id_kategori','1')
       	->paginate(8);
       	$kategori = $kategori1;

       	return view ('web-profile.webProfileLaptop')
        	->with (compact('kategori'));
    }

    public function getGaming()
    {
    	$kategori1 = \DB::table('barangs')
        ->select('barangs.nama_barang as nama_barang','barangs.id','barangs.harga','barangs.id_kategori','barangs.foto_barang','barangs.jumlah')
       	->where('barangs.id_kategori','2')
       	->paginate(8);
       	$kategori = $kategori1;

       	return view ('web-profile.webProfileGaming')
        	->with (compact('kategori'));
    }

    public function getNotebook()
    {
    	$kategori1 = \DB::table('barangs')
        ->select('barangs.nama_barang as nama_barang','barangs.id','barangs.harga','barangs.id_kategori','barangs.foto_barang','barangs.jumlah')
       	->where('barangs.id_kategori','3')
       	->paginate(8);
       	$kategori = $kategori1;

       	return view ('web-profile.webProfileNotebook')
        	->with (compact('kategori'));
    }

    public function getNetbook()
    {
    	$kategori1 = \DB::table('barangs')
        ->select('barangs.nama_barang as nama_barang','barangs.id','barangs.harga','barangs.id_kategori','barangs.foto_barang','barangs.jumlah')
       	->where('barangs.id_kategori','4')
       	->paginate(8);
       	$kategori = $kategori1;

       	return view ('web-profile.webProfileNetbook')
        	->with (compact('kategori'));
    }

    public function getMacbook()
    {
    	$kategori1 = \DB::table('barangs')
        ->select('barangs.nama_barang as nama_barang','barangs.id','barangs.harga','barangs.id_kategori','barangs.foto_barang','barangs.jumlah')
       	->where('barangs.id_kategori','5')
       	->paginate(8);
       	$kategori = $kategori1;

	     $barangs = \DB::table('barangs')->max('harga');
       	// $barangs = barang::count();
       	// $barangs = \DB::table('barangs')->sum('jumlah');

     //   	$barangs = \DB::table('barangs')
    	// ->where ('barangs.id_kategori','1');
    	// $barang = $barangs->sum('jumlah');

       	return view ('web-profile.webProfileMacbook')
        	->with (compact('kategori'))
        	->with (compact('barangs'));
    }

    public function getProfile()
    {
    	return view ('web-profile.webProfileTentang');
    }

    public function category ($category){
      $kategori1 = \DB::table('barangs')
        ->select('barangs.nama_barang as nama_barang','barangs.id','barangs.harga','barangs.id_kategori','barangs.foto_barang','barangs.jumlah')
        ->where('barangs.id_kategori','1')
        ->paginate(8);
        $kategori = $kategori1;

        return view ('web-profile.webProfileLaptop')
          ->with (compact('kategori'));

      $kategori2 = \DB::table('barangs')
        ->select('barangs.nama_barang as nama_barang','barangs.id','barangs.harga','barangs.id_kategori','barangs.foto_barang','barangs.jumlah')
        ->where('barangs.id_kategori','2')
        ->paginate(8);
        $kategori = $kategori2;

        return view ('web-profile.webProfileGaming')
          ->with (compact('kategori'));

      $kategori3 = \DB::table('barangs')
        ->select('barangs.nama_barang as nama_barang','barangs.id','barangs.harga','barangs.id_kategori','barangs.foto_barang','barangs.jumlah')
        ->where('barangs.id_kategori','3')
        ->paginate(8);
        $kategori = $kategori3;

        return view ('web-profile.webProfileNotebook')
          ->with (compact('kategori'));

      $kategori4 = \DB::table('barangs')
        ->select('barangs.nama_barang as nama_barang','barangs.id','barangs.harga','barangs.id_kategori','barangs.foto_barang','barangs.jumlah')
        ->where('barangs.id_kategori','4')
        ->paginate(8);
        $kategori = $kategori4;

        return view ('webProfileNetbook')
          ->with (compact('kategori'));

      $kategori5 = \DB::table('barangs')
        ->select('barangs.nama_barang as nama_barang','barangs.id','barangs.harga','barangs.id_kategori','barangs.foto_barang','barangs.jumlah')
        ->where('barangs.id_kategori','5')
        ->paginate(8);
        $kategori = $kategori5;

       $barangs = \DB::table('barangs')->max('harga');
        // $barangs = barang::count();
        // $barangs = \DB::table('barangs')->sum('jumlah');

     //     $barangs = \DB::table('barangs')
      // ->where ('barangs.id_kategori','1');
      // $barang = $barangs->sum('jumlah');

        return view ('web-profile.webProfileMacbook')
          ->with (compact('kategori'))
          ->with (compact('barangs'));

      //$tentang = return view ('webProfileTentang');

    }

    



}
