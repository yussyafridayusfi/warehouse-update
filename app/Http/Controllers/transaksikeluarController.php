<?php

namespace App\Http\Controllers;

use App\transaksi;
use App\customer;
use App\barang;
use App\User;
use App\logs;

//use App\Http\Controllers\DB;
use App\Http\Controllers\View;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Auth;
use App\Http\Controllers\userController;
use App\Http\Controllers\customerController;
use App\Http\Controllers\barangController;
use App\Http\Controllers\logsController;
use App\Http\Controllers\hitungHarga;
use Illuminate\Support\Facades\DB;

class transaksikeluarController extends Controller
{
     public function index()
    {
        // $transaksis = transaksi::all();
        // return view ('mastertransaksi',compact('transaksis'));

        $transaksis_keluar = \DB::table('transaksis')          
        -> join('customers', 'transaksis.id_customer', '=', 'customers.id')
        -> join('barangs', 'transaksis.id_barang', '=', 'barangs.id')
        -> join('users', 'transaksis.id_user', '=', 'users.id')
        -> select('transaksis.id','transaksis.jenistransaksi','transaksis.nomortransaksi','customers.nama as nama_customer','barangs.nama_barang as nama_barang','users.nama as nama_user','transaksis.jumlah','transaksis.harga','transaksis.total','transaksis.created_at','transaksis.updated_at')            
        ->where('transaksis.id_suplier', null) 
        // ->where('transaksis.jenistransaksi', 'barang-masuk') 
        // to get price value from barang table 
        //'barangs.harga'
        ->get();
        return view ('transaksi.mastertransaksi-keluar',compact('transaksis_keluar')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $maxId = \DB::table('transaksis')
        ->where('transaksis.id_suplier', null)
        ->max('id');
        $maxId= $maxId + 1;

       // $maxJumlah = \DB::table('barangs')->max('jumlah');

        $users = User::all();
        $customers = customer::all();
        $barangs = barang::all();
        return view ('transaksi.createtransaksi-keluar')
        ->with(compact('users'))
        ->with(compact('customers'))
        ->with(compact('barangs'))
        ->with(compact('maxId'));

        $harga = barang::where('id','=',$id_barang)->get();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $success = 'failed';
        $stok_old = DB::table('barangs')->where('id', $request->id_barang)->value('jumlah');
        $stok_new = ($stok_old-$request->jumlah);
        $user = \Auth::User()->jabatan;
        $jenis_logs = 'transaksi';
        
        $jenistransaksi = 'barang-keluar';
        
        $transaksi = transaksi::create([
            'nomortransaksi'=>$request->nomortransaksi,
            'jenistransaksi'=>$jenistransaksi,
            'id_customer'=> $request->id_customer,
            'id_barang'=> $request->id_barang,
            'id_user'=> $request->id_user,
            'jumlah'=>$request->jumlah,
            'harga'=>$request->harga,
            'total'=>$request->total
        ]); 
        DB::table('barangs')
            ->where('id', $request->id_barang)
            ->update(['jumlah' => $stok_new ]);

        $keterangan = 'pengurangan stock. jumlah yang dibeli customer = '.$request->jumlah;

        $logs = logs::create([
            'jenis_logs'=>$jenis_logs,
            'nama_logs'=>$jenistransaksi,
            'id_barang'=> $request->id_barang,
            'user'=> $user,
            'nama_target'=>$request->id_customer,
            'keterangan'=>$keterangan,
            'stok_lama'=>$stok_old,
            'stok_baru'=>$stok_new
        ]); 

        {
            $success = 'success';
        }
        

        return redirect()->route('transaksi-keluar.index')->with('alert', $success);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User::all();
        $customers = customer::all();
        $barangs = barang::all();
        $transaksis = transaksi::find($id);
    }

    public function CariHargaK(Request $request){
        // $p= barang::select('harga')->where('id',$request->id)->get();
        
        return response()->json(Barang::find($request->id)->harga);

    }

    public function CariJumlahK(Request $request){
        // $p= barang::select('harga')->where('id',$request->id)->get();
        
        return response()->json(Barang::find($request->id)->jumlah);

    }

    public function JenisBarang(){
        $transaksis = DB::table('transaksis_barang')->get();
        return response()->json ($transaksis);
    }
    
    public function uang(){
        $transaksis = DB::table('jumuang')->get();
        return response()->json ($transaksis);
    }

}
