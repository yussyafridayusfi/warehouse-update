<?php

namespace App\Http\Controllers;

use App\kategori;
use Illuminate\Http\Request;
use App\Http\Requests;

class kategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategoris = kategori::all();

        //hitung jumlah laptop
        $laptops = \DB::table('barangs')
        ->where ('barangs.id_kategori','1')
        ->where ('barangs.deleted_at', null);
        $laptop = $laptops->sum('jumlah');

        //hitung jumlah laptop-gaming
        $gamings = \DB::table('barangs')
        ->where ('barangs.id_kategori','2')
        ->where('barangs.deleted_at', null);
        $gaming = $gamings->sum('jumlah');

        //hitung jumlah notebook
        $notebooks = \DB::table('barangs')
        ->where ('barangs.id_kategori','3')
        ->where('barangs.deleted_at', null);
        $notebook = $notebooks->sum('jumlah');

        //hitung jumlah netbook
        $netbooks = \DB::table('barangs')
        ->where ('barangs.id_kategori','4')
        ->where('barangs.deleted_at', null);
        $netbook = $netbooks->sum('jumlah');

        //hitung jumlah macbook
        $macbooks = \DB::table('barangs')
        ->where ('barangs.id_kategori','5')
        ->where('barangs.deleted_at', null);
        $macbook = $macbooks->sum('jumlah');

        return view ('kategori.masterkategori')
        ->with (compact('kategoris'))
        // ->with (compact('countCustomer'))
        // ->with (compact('countSupplier'))
        ->with (compact('laptop'))
        ->with (compact('gaming'))
        ->with (compact('notebook'))
        ->with (compact('netbook'))
        ->with (compact('macbook'));
        // ->with (compact('sumBarang'))
        // ->with (compact('maxTrans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('kategori.createkategori');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = 'failed';
        $kategori = kategori::create([
            'nama_kategori'=> $request->nama_kategori
        ]); 

        {
           $success = 'success'; 
        }

        return redirect()->route('kategori.index')->with('alert', $success);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kategoris = kategori::find($id)->update($request->all());
        // $kategoris = kategori::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategoris = kategori::find($id);
        $kategori = $kategoris;
        return view('kategori.editkategori',compact('kategoris'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        kategori::where('id', $id)->update([
            'nama_kategori'=> $request->nama_kategori
        ]);

        return redirect()->route('kategori.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategoris = kategori::findOrFail($id);
        $kategoris->delete();

        return redirect()->route('kategori.index');
    }
}
