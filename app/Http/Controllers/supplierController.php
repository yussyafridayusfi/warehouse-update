<?php

namespace App\Http\Controllers;

use App\suplier;
use Illuminate\Http\Request;
use App\Http\Requests;

class supplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = suplier::all();
        return view ('supplier.mastersupplier',compact('suppliers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('supplier.createsupplier');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = 'failed';
        $suplier = suplier::create([
            'nama'=> $request->nama,
            'alamat'=> $request->alamat,
            'telp'=> $request->telp,
            'email'=>$request->email
        ]); 

        $this->validate($request,[
            'email'=>'required|email|unique:users'
        ]);

        {
           $success = 'success'; 
        }

        return redirect()->route('supplier.index')->with('alert', $success);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $suppliers = suplier::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $suppliers = suplier::find($id);
        $supplier = $suppliers;
        return view('supplier.editsupplier',compact('supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        suplier::where('id', $id)->update([
            'nama' => request('nama'),
            'alamat' => request('alamat'),
            'telp' => request('telp'),
            'email' => request('email')
        ]);

        return redirect()->route('supplier.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $suppliers = suplier::findOrFail($id);
        $suppliers->delete();

        return redirect()->route('supplier.index')
        ->with('delete','Anda telah berhasil menghapus supplier');
    }
}
