<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view ('user.masteruser',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('user.createuser');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fileName = time() . '.png';
        $user = User::create([
            'nama'=> $request->nama,
            'alamat'=> $request->alamat,
            'email'=>$request->email,
            'password'=>bcrypt($request->password),
            'telp'=> $request->telp,
            'foto_profil'=> $request->file('foto_profil')->storeAs('foto_profil',$fileName),
            'jabatan'=> $request->jabatan
        ]);

        $this->validate($request,[
            // 'email'=>'required|email|unique:users',
            'password'=>'required|min:6|confirmed',
            'foto_profil'=>'required|mimes:jpeg,jpg,png'
        ]);
        
        // auth()->loginUsingId($user->nama);
        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User::all();
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = User::find($id);
        $user = $users;
        return view('user.edituser',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $foto = User::where('id',$id)->pluck('foto_profil');
        // dd($foto);
        User::where('id', $id)->update([
            'nama'=> $request->nama,
            'alamat'=> $request->alamat,
            'email'=>$request->email,
            'password'=>bcrypt($request->password),
            'telp'=> $request->telp,
            // 'foto_profil'=> $request->file('foto_profil')!=null? $request->file('foto_profil')->store('photos'):$foto,
            'jabatan'=> $request->jabatan
        ]);
        //User::find($id)->update($request->all());
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
