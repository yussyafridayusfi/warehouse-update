<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    public function __construct()
    {
        $this->middleware('guest');
    }

     public function showRegistrationForm()
    {
        return view('createuser');
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();


        event(new Registered($user = User::create([
            'nama' => $request->get('nama'),
            'alamat' => $request->get('alamat'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'telp' => $request->get('telp'),
            'foto_profil' => $request->file('foto_profil')->store('photos'),
            'jabatan' => $request->get('jabatan'),
        ])));

        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'foto_profil'=>'required|mimes:jpeg,jpg,png',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function redirectPath()
    {
        return route('dashboard');
    }
}
