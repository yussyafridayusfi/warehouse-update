<?php

namespace App\Http\Controllers;

use App\transaksi;
use App\customer;
use App\barang;
use App\User;
use App\logs;
use App\suplier;

//use App\Http\Controllers\DB;
use App\Http\Controllers\View;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Auth;
use App\Http\Controllers\userController;
use App\Http\Controllers\customerController;
use App\Http\Controllers\barangController;
use App\Http\Controllers\supplierController;
use App\Http\Controllers\logsController;
use App\Http\Controllers\hitungHarga;
use Illuminate\Support\Facades\DB;

class transaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $transaksis = transaksi::all();
        // return view ('mastertransaksi',compact('transaksis'));

       $transaksis_masuk = \DB::table('transaksis')          
        -> join('supliers', 'transaksis.id_suplier', '=', 'supliers.id')
        -> join('barangs', 'transaksis.id_barang', '=', 'barangs.id')
        -> join('users', 'transaksis.id_user', '=', 'users.id')
        -> select('transaksis.id','transaksis.jenistransaksi','transaksis.nomortransaksi','supliers.nama as nama_suplier','barangs.nama_barang as nama_barang','users.nama as nama_user','transaksis.jumlah','transaksis.harga','transaksis.total','transaksis.created_at','transaksis.updated_at')            
        ->where('transaksis.id_customer', null) 
        ->get();
        return view ('transaksi.mastertransaksi-masuk',compact('transaksis_masuk')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $maxId = \DB::table('transaksis')
        ->where('transaksis.id_customer', null)
        ->max('id');
        $maxId= $maxId + 1;

       // $maxJumlah = \DB::table('barangs')->max('jumlah');

        $users = User::all();
        $suppliers = suplier::all();
        $barangs = barang::all();
        return view ('transaksi.createtransaksi-masuk')
        ->with(compact('users'))
        ->with(compact('suppliers'))
        ->with(compact('barangs'))
        ->with(compact('maxId'));

        $harga = barang::where('id','=',$id_barang)->get();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $success = 'failed';
        $stok_old = DB::table('barangs')->where('id', $request->id_barang)->value('jumlah');
        $stok_new = ($stok_old+$request->jumlah);
        $user = \Auth::User()->jabatan;
        $jenis_logs = 'transaksi';
        
        $jenistransaksi = 'barang-masuk';
        
        $transaksi = transaksi::create([
            'nomortransaksi'=>$request->nomortransaksi,
            'jenistransaksi'=>$jenistransaksi,
            'id_suplier'=> $request->id_suplier,
            'id_barang'=> $request->id_barang,
            'id_user'=> $request->id_user,
            'jumlah'=>$request->jumlah,
            'harga'=>$request->harga,
            'total'=>$request->total
        ]); 
        DB::table('barangs')
            ->where('id', $request->id_barang)
            ->update(['jumlah' => $stok_new ]);

        $keterangan = 'penambahan stock. jumlah yang disuplai supplier = '.$request->jumlah;

        $logs = logs::create([
            'jenis_logs'=>$jenis_logs,
            'nama_logs'=>$jenistransaksi,
            'id_barang'=>$request->id_barang,
            'user'=>$user,
            'nama_target'=>$request->id_suplier,
            'keterangan'=>$keterangan,
            'stok_lama'=>$stok_old,
            'stok_baru'=>$stok_new
            
        ]); 

        {
            $success = 'success';
        }
        

        return redirect()->route('transaksi-masuk.index')->with('alert', $success);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User::all();
        $customers = customer::all();
        $barangs = barang::all();
        $transaksis = transaksi::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function CariHarga(Request $request){
        // $p= barang::select('harga')->where('id',$request->id)->get();
        
        return response()->json(Barang::find($request->id)->harga);

    }

    public function CariJumlah(Request $request){
        // $p= barang::select('harga')->where('id',$request->id)->get();
        
        return response()->json(Barang::find($request->id)->jumlah);

    }

}
