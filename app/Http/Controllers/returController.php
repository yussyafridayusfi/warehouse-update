<?php

namespace App\Http\Controllers;

use App\barang;
use App\retur;
use App\transaksi;
use App\kategori;
use App\logs;
use App\suplier;
use App\Http\Auth;
//use App\Http\Controllers\DB;
use App\Http\Controllers\View;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\barangController;
use App\Http\Controllers\transaksiController;
use App\Http\Controllers\LogsController;
use App\Http\Controllers\supplierController;
use Illuminate\Support\Facades\DB;

class returController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $returs = retur::all();
        // return view ('masterretur',compact('returs'));

        $returs = \DB::table('returs')        
        -> join('transaksis', 'returs.id_transaksi', '=', 'transaksis.id')
        -> join('customers', 'returs.nama_target', '=', 'customers.id')
        -> join('barangs', 'returs.id_barang', '=', 'barangs.id')
        -> select('returs.id','transaksis.nomortransaksi','barangs.nama_barang as nama_barang','returs.jumlah','returs.keterangan','customers.nama as nama_target','returs.created_at','returs.updated_at')            
        ->where('returs.jenisretur', 'barang-keluar') 
        ->get();
        return view ('retur.masterretur-keluar',compact('returs')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $transaksis = transaksi::where('transaksis.jenistransaksi','barang-keluar')->get();
        $returs = retur::all();
        $barangs= barang::all();
        return view ('retur.createretur-keluar')
        ->with(compact('transaksis'))
        ->with(compact('returs'))
        ->with(compact('barangs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = 'failed';
        $user = \Auth::User()->jabatan;
        $jenis_logs = 'retur';
        $jenisretur = 'barang-keluar';

        $stok_old = DB::table('barangs')->where('id', $request->id_barang)->value('jumlah');
        $stok_new = ($stok_old+$request->jumlah);
        $retur = retur::create([
            'jenisretur'=> $jenisretur,
            'id_transaksi'=> $request->id_transaksi,
            'id_barang'=> $request->id_barang,
            'jumlah'=>$request->jumlah,
            'keterangan'=>$request->keterangan,
            'nama_target'=>$request->id_customer
        ]); 
         DB::table('barangs')
            ->where('id', $request->id_barang)
            ->update(['jumlah' => $stok_new ]);

        $logs = logs::create([
            'jenis_logs'=>$jenis_logs,
            'nama_logs'=>$jenisretur,
            'id_barang'=> $request->id_barang,
            'user'=> $user,
            'nama_target'=>$request->id_customer,
            'keterangan'=>$request->keterangan,
            'stok_lama'=>$stok_old,
            'stok_baru'=>$stok_new
        ]); 
         
        {
            $success = 'success';
        }

        return redirect()->route('retur-keluar.index')->with('alert', $success);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $returs = retur::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function CariBarang(Request $request)
    {
        return response()->json(transaksi::find($request->id)->id_barang);
    }

    public function CariJumlahRetur(Request $request)
    {
        return response()->json(transaksi::find($request->id)->jumlah);
    }

    public function CariCustRetur(Request $request)
    {
        return response()->json(transaksi::find($request->id)->id_customer);
    }
}
