<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
// use App\Http\Controllers\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\profile;
use App\barang;

class authController extends Controller
{
    public function getLogin(){
        return view('index');
    }

    public function postLogin(Request $request){

        $user = User::where('nama' , $request->nama)->first();
        if(!$user)
        {
            return redirect()->back()->withInput()->with('info', 'Username anda salah');
        }

        if(!Hash::check($request->password, $user->password))
        {
            return redirect()->back()->withInput()->with('info', 'Password anda salah');
        }
        
        Auth::attempt($request->only('nama', 'password'));
        $user = auth()->user();

        return redirect()->route('dashboard.dashboard');
        
         
         // if(!\Auth::attempt(['nama'=>$request->nama, 'password'=>$request->password])){
         //    // dd('login not okay ');
         //    return redirect()->back();
         // }
        // dd('login okay ');

        //dd(!\Auth::attempt(['email'=>$request->nama, 'password'=>$request->password]));
        
        /*if(!\Auth::attempt(['nama'=>$request->nama,'password'=>$request->password])){
            return redirect()->back();
        }*/

        // return redirect()->route('dashboard.dashboard');
        
    }

    public function getRegister(){
       // return view ('register');
       return view ('user.createuser');
    }

    public function postRegister(Request $request){
     
        
        $user = User::create([
            'nama'=> $request->nama,
            'alamat'=> $request->alamat,
            'email'=>$request->email,
            'password'=>bcrypt($request->password),
            'telp'=> $request->telp,
            'foto_profil'=> $request->file('foto_profil')->store('photos'),
            'jabatan'=> $request->jabatan
        ]);

        $this->validate($request,[
           // 'email'=>'required|email',
            'password'=>'required|min:6|confirmed',
            'foto_profil'=>'required|mimes:jpeg,jpg,png'
        ]);

        return redirect()->back()->with('alert', $success);
        // auth()->loginUsingId($user->nama);
        // return redirect()->route('login');
        
        
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('login');
        // auth()->logout();

        // return redirect()->route('login');
    }

    public function masterUser(){
        $users = User::all();
        return view('user.masteruser',compact('users'));

       //$user = User::whereId($id)->firstOrFail(['id', 'firstname', 'lastname', 'avatar', 'username', 'email', 'password', 'level', 'status'])->toArray();
        //return view('layout.masterUser')->with('user', $user);
    }
    
}
