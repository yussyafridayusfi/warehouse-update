<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class suplier extends Model
{
    protected $fillable = [
        'nama', 'alamat', 'telp', 'email',
    ];

    public function barang()
    {
        return $this->hasMany(barang::class);
    }
    
    use SoftDeletes;
    protected $dates =['deleted_at'];
}
