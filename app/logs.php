<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class logs extends Model
{
    protected $fillable = [
        'jenis_logs','id_barang','user','nama_logs','nama_target','keterangan','stok_lama','stok_baru',
    ];

    public function barang()
    {
        return $this->belongsTo(barang::class);   
    }  
    
    use SoftDeletes;
    protected $dates =['deleted_at'];

}
