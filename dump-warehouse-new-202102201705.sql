-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: localhost    Database: warehouse-new
-- ------------------------------------------------------
-- Server version	8.0.23-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `barangs`
--

DROP TABLE IF EXISTS `barangs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `barangs` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nama_barang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_barang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_kategori` int unsigned NOT NULL,
  `jumlah` int NOT NULL,
  `harga` int NOT NULL,
  `foto_barang` text COLLATE utf8mb4_unicode_ci,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `barangs_id_kategori_foreign` (`id_kategori`),
  CONSTRAINT `barangs_id_kategori_foreign` FOREIGN KEY (`id_kategori`) REFERENCES `kategoris` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barangs`
--

LOCK TABLES `barangs` WRITE;
/*!40000 ALTER TABLE `barangs` DISABLE KEYS */;
INSERT INTO `barangs` VALUES (1,'Numquam quae numquam','Brg1',1,78,58,'foto_barang/WA4BW7d6nRjzgerMCaDOZT2rTCKxOBoChqgihmxi.png','Enim nemo vero conse',NULL,'2021-02-19 06:22:26','2021-02-19 07:20:00'),(2,'Unde est qui reicie','Brg2',1,6,25,'foto_barang/KkWN0M88rsLzVyYziG2hMJNrqv7O0bBIILtLKM6Z.png','Impedit aliquip rem',NULL,'2021-02-19 06:52:56','2021-02-19 06:52:56'),(3,'Ut deserunt asperior','Brg3',2,68,65,'foto_barang/NEZcmJxX5prZrjBg1buVJOLTmwPTNqvolNqrqU9n.png','Nihil dignissimos es',NULL,'2021-02-19 21:27:49','2021-02-19 21:27:49'),(4,'Molestiae minus null','Brg4',2,18,13,'foto_barang/eb5upfT7X6lNapnzBEOIswtwL1s44exkX8wrNfMQ.png','Libero incidunt nos',NULL,'2021-02-19 21:30:23','2021-02-19 21:30:23'),(5,'Qui fuga Beatae nes','Brg5',1,82,43,'foto_barang/nfslv1nqo2yXVT2tdqH7GbC95ZrkSNSsB3gBuJpf.png','Deserunt reprehender',NULL,'2021-02-19 21:51:28','2021-02-19 21:51:28'),(6,'Dolores deserunt qui','Brg6',2,70,8,'foto_barang/5Z4AsNn9SjfUsbwgh3kSBia1UeIQ0nLO28cU5Zr0.png','Consequatur sequi a',NULL,'2021-02-19 22:05:01','2021-02-19 22:32:13'),(7,'Acer','Brg7',1,92,360000,'foto_barang/w1YTChJC2HhwF14iF7LCyjXHU2j1XM0m0utOK4C9.jpeg','Sunt id in mollit ei',NULL,'2021-02-19 22:58:24','2021-02-19 22:58:24'),(8,'HP','Brg8',2,74,6600000,'foto_barang/OcNUoMNqRbO4FAmz04LQvXL1ZQqXWCquEMAHUw5Z.jpeg','Nihil anim consectet',NULL,'2021-02-19 22:59:11','2021-02-19 22:59:11'),(9,'Dell 4563','Brg9',1,27,7700000,'foto_barang/kk2jZOrShSG7cXHLb5ZdzGzUTOpSvTfanP83RzMW.jpeg','Eaque sit impedit i',NULL,'2021-02-19 22:59:38','2021-02-19 22:59:38'),(10,'Acer 6243','Brg10',2,5,6400000,'foto_barang/TDOBhBS3ZRmWuyIPE5bQCsAeGxl8jXTuqnqIbeF9.jpeg','Nostrud reprehenderi',NULL,'2021-02-19 23:00:11','2021-02-19 23:00:11'),(11,'Macbook 3523','Brg11',5,17,750000,'foto_barang/Z1gTusQNAsrSz0WIvRbGduUjkzyufR12KzykmiCo.jpeg','Nobis ea sequi delen',NULL,'2021-02-19 23:00:38','2021-02-19 23:00:38'),(12,'HP 4523','Brg12',3,12,2600000,'foto_barang/lfhX10OdepsZmIuXNBEuwfOt0uqpXf012q0UpbaS.jpeg','Dolores impedit des',NULL,'2021-02-19 23:01:09','2021-02-19 23:01:09'),(13,'Dell 7645','Brg13',3,74,7100000,'foto_barang/KlYovzgR8681sApYix9I2EGGvhcQFZxuDOxyE7Gf.jpeg','Quia cum omnis minim',NULL,'2021-02-19 23:01:26','2021-02-19 23:01:26'),(14,'Acer 4542','Brg14',4,58,5600000,'foto_barang/F5Jk3RiPH3jfxC1D2PFcLk31FbZRFN1U8faM8ONj.jpeg','Et aut aut quo moles',NULL,'2021-02-19 23:01:46','2021-02-19 23:01:46'),(15,'Asus 3422','Brg15',4,54,5400000,'foto_barang/isEjuKt4MB3Weakzvxg5JKdEeVQj14xBHPf7n409.jpeg','Sunt voluptatem Eaq',NULL,'2021-02-19 23:02:09','2021-02-19 23:02:09'),(16,'Macbook 5645','Brg16',5,49,37000000,'foto_barang/HuO4STx9q4BDxFH8rA6qTiGZpWBrPRDXJAssQFEP.jpeg','Qui ducimus laudant',NULL,'2021-02-19 23:02:34','2021-02-19 23:02:34');
/*!40000 ALTER TABLE `barangs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customers` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `telp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'Voluptas corrupti e','Minima aut omnis a a','(122) 789-736-193',NULL,'2021-02-19 06:21:27','2021-02-19 06:21:27'),(2,'Voluptatum voluptate','Dolores distinctio','(128) 570-199-734',NULL,'2021-02-19 07:32:17','2021-02-19 07:32:17');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategoris`
--

DROP TABLE IF EXISTS `kategoris`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `kategoris` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategoris`
--

LOCK TABLES `kategoris` WRITE;
/*!40000 ALTER TABLE `kategoris` DISABLE KEYS */;
INSERT INTO `kategoris` VALUES (1,'Laptop',NULL,'2021-02-19 06:21:41','2021-02-19 22:55:33'),(2,'Laptop - Gaming',NULL,'2021-02-19 06:57:05','2021-02-19 22:55:44'),(3,'Notebook',NULL,'2021-02-19 22:55:56','2021-02-19 22:55:56'),(4,'Netbook',NULL,'2021-02-19 22:56:34','2021-02-19 22:56:34'),(5,'Macbook',NULL,'2021-02-19 22:56:46','2021-02-19 22:56:46');
/*!40000 ALTER TABLE `kategoris` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `logs` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `id_barang` int unsigned NOT NULL,
  `user` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_logs` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_logs` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `stok_lama` int NOT NULL,
  `stok_baru` int NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `logs_id_barang_foreign` (`id_barang`),
  CONSTRAINT `logs_id_barang_foreign` FOREIGN KEY (`id_barang`) REFERENCES `barangs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
INSERT INTO `logs` VALUES (1,1,'superadmin','transaksi','barang-masuk','1','penambahan stock. jumlah yang disuplai supplier = 3',84,87,NULL,'2021-02-19 06:29:36','2021-02-19 06:29:36'),(2,1,'superadmin','transaksi','barang-keluar','1','pengurangan stock. jumlah yang dibeli customer = 7',87,80,NULL,'2021-02-19 06:30:03','2021-02-19 06:30:03'),(3,1,'superadmin','transaksi','barang-masuk','1','penambahan stock. jumlah yang disuplai supplier = 5',83,88,NULL,'2021-02-19 07:11:21','2021-02-19 07:11:21'),(4,1,'superadmin','transaksi','barang-masuk','1','penambahan stock. jumlah yang disuplai supplier = 2',80,82,NULL,'2021-02-19 07:41:45','2021-02-19 07:41:45'),(5,1,'superadmin','transaksi','barang-keluar','1','pengurangan stock. jumlah yang dibeli customer = 3',82,79,NULL,'2021-02-19 08:02:42','2021-02-19 08:02:42'),(6,1,'superadmin','retur','barang-masuk','1','hkkkkkkkj',79,77,NULL,'2021-02-19 08:16:40','2021-02-19 08:16:40'),(7,1,'superadmin','retur','barang-masuk','1','dfssd',77,76,NULL,'2021-02-19 08:31:55','2021-02-19 08:31:55'),(8,1,'superadmin','retur','barang-keluar','1','esdsda',76,78,NULL,'2021-02-19 08:32:33','2021-02-19 08:32:33'),(9,8,'owner','transaksi','barang-masuk','1','penambahan stock. jumlah yang disuplai supplier = 2',73,75,NULL,'2021-02-19 23:03:19','2021-02-19 23:03:19'),(10,11,'owner','transaksi','barang-keluar','2','pengurangan stock. jumlah yang dibeli customer = 1',18,17,NULL,'2021-02-19 23:03:47','2021-02-19 23:03:47'),(11,8,'owner','retur','barang-masuk','1','uhjhkg',75,74,NULL,'2021-02-19 23:04:18','2021-02-19 23:04:18');
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (28,'2014_10_12_000000_create_users_table',1),(29,'2014_10_12_100000_create_password_resets_table',1),(30,'2018_09_08_233651_create_customers_table',1),(31,'2018_09_08_233918_create_supliers_table',1),(32,'2018_09_08_234042_create_kategoris_table',1),(33,'2018_09_08_234214_create_barangs_table',1),(34,'2018_09_08_235146_create_transaksis_table',1),(35,'2018_09_08_235732_create_returs_table',1),(36,'2019_07_09_111329_create_logs_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `returs`
--

DROP TABLE IF EXISTS `returs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `returs` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `jenisretur` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_transaksi` int unsigned NOT NULL,
  `id_barang` int unsigned NOT NULL,
  `jumlah` int NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `returs_id_transaksi_foreign` (`id_transaksi`),
  KEY `returs_id_barang_foreign` (`id_barang`),
  CONSTRAINT `returs_id_barang_foreign` FOREIGN KEY (`id_barang`) REFERENCES `barangs` (`id`),
  CONSTRAINT `returs_id_transaksi_foreign` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksis` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `returs`
--

LOCK TABLES `returs` WRITE;
/*!40000 ALTER TABLE `returs` DISABLE KEYS */;
INSERT INTO `returs` VALUES (1,'barang-masuk',1,1,2,'hkkkkkkkj','1','2021-02-19 08:16:40','2021-02-19 08:16:40'),(2,'barang-masuk',1,1,1,'dfssd','1','2021-02-19 08:31:55','2021-02-19 08:31:55'),(3,'barang-keluar',2,1,2,'esdsda','1','2021-02-19 08:32:33','2021-02-19 08:32:33'),(4,'barang-masuk',6,8,1,'uhjhkg','1','2021-02-19 23:04:18','2021-02-19 23:04:18');
/*!40000 ALTER TABLE `returs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supliers`
--

DROP TABLE IF EXISTS `supliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `supliers` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `telp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `supliers_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supliers`
--

LOCK TABLES `supliers` WRITE;
/*!40000 ALTER TABLE `supliers` DISABLE KEYS */;
INSERT INTO `supliers` VALUES (1,'Tempor harum duis de','Perferendis et nihil','(342) 342-342-342','roladu@mailinator.com',NULL,'2021-02-19 06:20:48','2021-02-19 06:20:48'),(2,'Accusamus','Eu explicabo Nobis','(423) 523-423-424','hadi@mailinator.com','2021-02-19 07:25:43','2021-02-19 07:23:09','2021-02-19 07:25:43'),(3,'Nihil labore aut deb','Ut nihil reprehender','(223) 123-131-323','lohyn@mailinator.com',NULL,'2021-02-19 07:24:10','2021-02-19 07:24:10'),(4,'Nemo in est qui eni','Voluptatem tenetur a','(342) 434-234-342','sifumule@mailinator.com',NULL,'2021-02-19 07:24:19','2021-02-19 07:24:19'),(5,'Cumque non ut totam','Ex cum ut eaque aliq','(342) 434-243-423','kysewapygi@mailinator.com',NULL,'2021-02-19 07:24:58','2021-02-19 07:24:58');
/*!40000 ALTER TABLE `supliers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksis`
--

DROP TABLE IF EXISTS `transaksis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaksis` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nomortransaksi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenistransaksi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_barang` int unsigned NOT NULL,
  `id_customer` int unsigned DEFAULT NULL,
  `id_suplier` int unsigned DEFAULT NULL,
  `id_user` int unsigned NOT NULL,
  `jumlah` int NOT NULL,
  `harga` int NOT NULL,
  `total` int NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transaksis_id_customer_foreign` (`id_customer`),
  KEY `transaksis_id_barang_foreign` (`id_barang`),
  KEY `transaksis_id_user_foreign` (`id_user`),
  KEY `transaksis_id_suplier_foreign` (`id_suplier`),
  CONSTRAINT `transaksis_id_barang_foreign` FOREIGN KEY (`id_barang`) REFERENCES `barangs` (`id`),
  CONSTRAINT `transaksis_id_customer_foreign` FOREIGN KEY (`id_customer`) REFERENCES `customers` (`id`),
  CONSTRAINT `transaksis_id_suplier_foreign` FOREIGN KEY (`id_suplier`) REFERENCES `supliers` (`id`),
  CONSTRAINT `transaksis_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksis`
--

LOCK TABLES `transaksis` WRITE;
/*!40000 ALTER TABLE `transaksis` DISABLE KEYS */;
INSERT INTO `transaksis` VALUES (1,'TransMsk1','barang-masuk',1,NULL,1,2,3,58,174,NULL,'2021-02-19 06:29:36','2021-02-19 06:29:36'),(2,'TransKlr1','barang-keluar',1,1,NULL,2,7,58,406,NULL,'2021-02-19 06:30:03','2021-02-19 06:30:03'),(3,'TransMsk2','barang-masuk',1,NULL,1,2,5,58,290,NULL,'2021-02-19 07:11:21','2021-02-19 07:11:21'),(4,'TransMsk4','barang-masuk',1,NULL,1,2,2,58,116,NULL,'2021-02-19 07:41:45','2021-02-19 07:41:45'),(5,'TransKlr3','barang-keluar',1,1,NULL,2,3,58,174,NULL,'2021-02-19 08:02:42','2021-02-19 08:02:42'),(6,'TransMsk5','barang-masuk',8,NULL,1,8,2,6600000,13200000,NULL,'2021-02-19 23:03:19','2021-02-19 23:03:19'),(7,'TransKlr6','barang-keluar',11,2,NULL,4,1,750000,750000,NULL,'2021-02-19 23:03:47','2021-02-19 23:03:47');
/*!40000 ALTER TABLE `transaksis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto_profil` text COLLATE utf8mb4_unicode_ci,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'yus123','rte','fovehaxyr@mailinator.com','$2y$10$CWmyBQMXPUhZvOIUcNBXOO5XLNlSP7grs2f2G.G9aFll2BlTRHHqi','(352) 343-242-432','photos/r8lNE13aV8ruEtkgXOoUjEOpvfuuJEa9h4dOti8F.png','superadmin',NULL,'2021-02-19 06:18:11','2021-02-19 07:31:58'),(3,'Omnis r','Culpa soluta id vo','siconahiz@mailinator.com','$2y$10$w5Eoq8Ga9v0I/CwMsqhK7e7/0wfXVDAHNG3q9WkXET5/MO9m4fP/O','(231) 443-424-323','photos/K7i2AEnxn3AdSj6gfdENeGjs1ymFotDTgsiQ37ta.png','owner',NULL,'2021-02-19 07:31:42','2021-02-19 08:59:51'),(4,'Sequi iste corporis','Omnis non aliquam vo','laticevuje@mailinator.com','$2y$10$ITSl3WO3l0IglLqLoCnw9eLoTLzm/uOhtX0FtqcDapHgVvHAIcl8u','(342) 343-424-324','photos/ngSfPdYIjR3WamGSRlr2dXIUUICzGRopK9EQFPpZ.png','superadmin',NULL,'2021-02-19 08:59:35','2021-02-19 08:59:35'),(5,'yusyus','Sit qui veritatis co','qicahuqyz@mailinator.com','$2y$10$hEowXhXT7t793JNBeUtal.L6PiO1jxUdtAPQeMh.XFP5.fzjs1FhC','(657) 534-753-642','photos/6oEOg8ecbazVe6vgTXdbhqeceWRVmEgcyy2BkpTW.png','owner',NULL,'2021-02-19 09:27:39','2021-02-19 09:27:39'),(6,'yussyaf','Atque esse natus dol','zudi@mailinator.com','$2y$10$PnT3qde53K9yn0O0HZ2Npe0QdkkOL16BMobofnls4UsI0FuMGn5Ry','(352) 354-634-673','photos/656XhveGEsHYheaIKFXFyY8x9IuANGu2CdJXMNXr.png','owner',NULL,'2021-02-19 09:34:34','2021-02-19 09:34:34'),(7,'user123','password user123','kavus@mailinator.com','$2y$10$8ZdpEb10oGkHazL5bO1qDOdjASWuecM.DGWSFWTCBMH8KcaOIQ5hy','(241) 354-546-532','foto_profil/HpK4OlnyxzBJKYSNgWG0Ao5SaSsqnMSeVZO8dJZg.png','owner',NULL,'2021-02-19 21:25:15','2021-02-19 21:25:15'),(8,'Fugiat maiores libe','Voluptas molestiae l','roju@mailinator.com','$2y$10$hof97F0Zz99PCLXb7ZUAM.jvbjUC/sssDCAKy/U0t0CAcOJkJBhH2','(525) 323-424-342','foto_profil/FXSIz1Fk7pOBbFzB7QPnINgcyHbJ78wudCMSRYME.png','admin',NULL,'2021-02-19 21:44:43','2021-02-19 22:43:53'),(9,'Harum voluptate labo','Cupidatat elit offi','xivu@mailinator.com','$2y$10$COPMB6AfFsuioP2mxJFyfOsNYRnXrA1t00G4XIPpPrMsaXpXwonNS','(143) 143-124-143','foto_profil/4RtwPnAOhPnP7LYQT7D4xRY9y7S09ODPuEUqWog3.png','admin',NULL,'2021-02-19 21:47:04','2021-02-19 22:44:26'),(10,'Ratione nostrum volu','Iusto dolore enim po','karoxy@mailinator.com','$2y$10$CZTmzn.A4xnJf/G4lXlwo.Bxf77cAkzCCKZOAwMsVKqpWjrAdZu6i','(352) 343-252-532','public/foto_profil/1613797170.png','superadmin',NULL,'2021-02-19 21:59:30','2021-02-19 21:59:30'),(12,'Suscipit elit fuga','Et harum voluptate o','ceqog@mailinator.com','$2y$10$Kp/i.VauOnDbAFu/odf8xOUcugMQIRFLjhXvGvAyYefBz.mMpDzki','(253) 634-643-646','foto_profil/1613797430.png','owner',NULL,'2021-02-19 22:03:50','2021-02-19 22:03:50');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'warehouse-new'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-20 17:05:48
