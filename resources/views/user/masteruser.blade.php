@extends('layout.default')

@section('title')
    Master User
@endsection

@section('content')
    <div class="title_right">
        <h3><a href="{{route('user.create')}}" class="btn-index btn btn-primary pull-right col-sm-2 col-sm-10" title="Add"><i class="glyphicon glyphicon-plus"></i></a></h3>
    </div>
    <div class="title_left">
        <h3>Master User</h3>
    </div>

    <div class="x_panel">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Membership</a></li>
            <li class="breadcrumb-item active"><a href="">User</a></li>
        </ol>
        <div class="con">
            <table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="datatable-buttons_info" style="width: 1031px;">
                <thead>
                <tr>
                    <th scope="col">Nama</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Email</th>
                    <th scope="col">Telp</th>
                    <th scope="col">Foto</th>
                    <th scope="col">Jabatan</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                <tr>
                    <td>{{ $user->nama }}</td>       
                    <td>{{ $user->alamat }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->telp }}</td>
                    <td>
                        <img src="{{ asset('storage/'.$user->foto_profil) }}" alt="image" height="75">
                    </td>
                    <td>{{ $user->jabatan }}</td>
                    <td>
                        <div id="lihat-modal-{{ $user->id}}" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">
                                            <!-- Beri id "modal-title" untuk tag span pada judul modal -->
                                            <span id="modal-title">View Master User</span>
                                        </h4>
                                    </div>
                                    <form action="{{ route ('user.show', $user->id )}}" method="GET">
                                        <div class="modal-body" value="">
                                        

                                        <div class="form-group">
                                            <label>ID</label>
                                            <div class="">
                                                <input type="text" class="form-control" id="id" name="id" value="{{ $user->id }}" disabled="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama</label>
                                            <div>
                                               <input type="text" class="form-control" id="nama" name="nama" value="{{ $user->nama }}" disabled=""> 
                                            </div>                                            
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat</label>
                                            <div>
                                              <input type="text" class="form-control" id="alamat" name="alamat" value="{{ $user->alamat }}" disabled="">  
                                            </div>                                            
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <div>
                                               <input type="text" class="form-control" id="email" name="email" value="{{ $user->email }}"disabled="" > 
                                            </div>                                         
                                        </div>
                                        <div class="form-group">
                                            <label>Telp</label>
                                            <div>
                                                <input type="text" class="form-control" id="telp" name="telp" value="{{ $user->email }}"disabled="" >
                                            </div>                                        
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Jabatan</label>
                                            <div>
                                              <input type="text" class="form-control" id="jabatan" name="jabatan" value="{{ $user->jabatan }}" disabled="">  
                                            </div>              
                                        </div>
                                        <div class="form-group">
                                            <label>Date Created</label>
                                            <div>
                                              <input type="text" class="form-control" id="created_at" name="created_at" value="{{ $user->created_at }}" disabled="">  
                                            </div>                      
                                        </div>
                                        <div class="form-group">
                                            <label>Last Modifield</label>
                                            <div>
                                               <input type="text" class="form-control" id="updated_at" name="updated_at" value="{{ $user->updated_at }}" disabled=""> 
                                            </div>
                                            
                                        </div>
                                        <div class="form-group">
                                            <label>Foto</label><br>
                                            <div>
                                               <img src="{{ asset('storage/'.$user->foto_profil) }}" alt="image" height="128"> 
                                            </div>
                                        </div>

                                    </div>
                                </form>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#"  data-toggle="modal" data-target="#lihat-modal-{{ $user->id  }}" class="btn btn-primary glyphicon glyphicon-eye-open" title="View"></a>
                        <a href="{{route('user.edit',$user->id)}}" class="btn btn-info glyphicon glyphicon-edit" title="Edit"></a>
                    </td>
                </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection