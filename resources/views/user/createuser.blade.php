@extends('layout.default')

@section('title')
  Master User - Add
@endsection

@section('content')
  <div class="title_right">
    <h3><a href="{{route('user.index')}}" class="btn-index btn btn-primary pull-right col-sm-2 col-sm-10" title="Back"><i class="fa fa-arrow-left"></i></a></h3>
  </div>
  <div class="title_left">
    <h3>Master User - Add</h3>
  </div>
  <div class="x_panel">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Membership</a></li>
      <li class="breadcrumb-item"><a href="#">User</a></li>
      <li class="breadcrumb-item active"><a href="">Add</a></li>
    </ol>
    <div class="x_content">
      @if(session('alert'))
        <div class="alert {{ session('alert') == 'success' ? 'alert-success' : 'alert-danger' }}">
            <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>@if(session('alert') == 'success') User Berhasil Ditambahkan. @else User Gagal Ditambahkan. @endif</span>
        </div>
        @endif
        <form method="post" action="{{route ('user.store')}}" class="form-horizontal form-label-left" role="form" name="_token" enctype="multipart/form-data">
          @if($errors->isNotEmpty())
                    <div class="alert alert-warning" role="alert">
                        @foreach($errors->all() as $error)
                            <span>{{ $error }}</span>
                            <br>
                        @endforeach
                    </div>
                @endif
        {{csrf_field()}}

        <div class="form-group">
            <label class="control-label col-sm-3 col-xs-12">Nama</label>
            <div class="col-sm-5 col-xs-12">
                <input type="text" id="nama" name="nama" value="{{old('nama')}}" required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">
                @if ($errors->has('nama'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nama') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3 col-xs-12">Alamat</label>
            <div class="col-sm-5 col-xs-12">
                <textarea id="alamat" name="alamat" class="form-control col-md-7 col-xs-12" rows="5"></textarea>
                @if ($errors->has('alamat'))
                  <span class="help-block">
                      <strong>{{ $errors->first('alamat') }}</strong>
                  </span>
              @endif
            </div>
        </div>

        <div class="form-group">
          <label class="control-label col-sm-3 col-xs-12">Email</label>
          <div class="col-sm-5 col-xs-12">
              <input type="email" id="email" name="email" value="{{old('email')}}" required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">
              @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-sm-3 col-xs-12">Password</label>
          <div class="col-sm-5 col-xs-12">
              <input type="password" id="password" name="password" value="{{old('password')}}" required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">
              @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-sm-3 col-xs-12">Password Konfirmasi</label>
          <div class="col-sm-5 col-xs-12">
              <input type="password" id="password_confirmation" name="password_confirmation" value="{{old('password_confirmation')}}" required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">
              @if ($errors->has('password_confirmation'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password_confirmation') }}</strong>
                  </span>
              @endif
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-sm-3 col-xs-12">Telp</label>
          <div class="col-sm-5 col-xs-12">
              <input type="text" class="form-control" data-inputmask="'mask' : '(999) 999-999-999'" id="telp" name="telp" value="{{old('telp')}}" required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">
              @if ($errors->has('telp'))
                  <span class="help-block">
                      <strong>{{ $errors->first('telp') }}</strong>
                  </span>
              @endif
              <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
          </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3 col-xs-12">Foto Profil</label>
            <div class="col-sm-5 col-xs-7">
                <input id="foto_profil" type="file" class="form-control" onchange="validate(this.value)" name="foto_profil"">
                @if ($errors->has('foto_profil'))
                    <span class="help-block">
                        <strong>{{ $errors->first('foto_profil') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
          <label class="control-label col-sm-3 col-xs-12">Jabatan</label>
          <div class="col-sm-3 col-sm-9">
            <select class="form-control" name="jabatan" id="jabatan">
              <option value="">-</option>
              <option value="superadmin">Super Admin</option>
              <option value="admin">Admin</option>
              <option value="owner">Owner</option>
            </select>
          </div>
        </div>
        <button class="btn btn-primary" type="submit">Add</button>
      </form>
    </div>
  </div>
@endsection
@section('script')
  <script type="text/javascript">
    function validate(file){
      var ext = file.substr(file.lastIndexOf('.')+1);
      var allow=["jpg","jpeg",'png','bmp'];
      if(allow.lastIndexOf(ext)==-1){
        $(".err").html('ERROR! The File Must Be Image!');
      }
    }
  </script>
@endsection