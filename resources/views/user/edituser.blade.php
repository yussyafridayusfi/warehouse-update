@extends('layout.default')

@section('title')
    Master User - Edit
@endsection

@section('content')
    <div class="title_right">
        <h3><a href="{{route('user.index')}}" class="btn-index btn btn-primary pull-right col-sm-2 col-sm-10" title="Back"><i class="fa fa-arrow-left"></i></a></h3>
    </div>
    <div class="title_left">
        <h3>Master User - Edit</h3>
    </div>
    <div class="x_panel">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Membership</a></li>
            <li class="breadcrumb-item"><a href="#">User</a></li>
            <li class="breadcrumb-item active"><a href="">Edit</a></li>
        </ol>
        <div class="x_content">
            @if(session('alert'))
                <div class="alert {{ session('alert') == 'success' ? 'alert-success' : 'alert-danger' }}">
                    <span>@if(session('alert') == 'success') User Berhasil Diedit. @else User Gagal Diedit. @endif</span>
                </div>
            @endif

            <form method="post" action="{{route ('user.update', $user->id)}}" class="form-horizontal form-label-left" role="form" name="_token" enctype="multipart/form-data">
                {{ method_field('PUT') }}
                {{csrf_field()}}
                

                <div class="form-group">
                    <label class="control-label col-sm-3 col-xs-12">Nama</label>
                    <div class="col-sm-7 col-xs-12">
                        <input type="text" id="nama" name="nama" value="{{ $user->nama }}" required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">
                        @if ($errors->has('nama'))
                            <span class="help-block">
                        <strong>{{ $errors->first('nama') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3 col-xs-12">Alamat</label>
                    <div class="col-sm-7 col-xs-12">
                        <textarea id="alamat" name="alamat" class="form-control col-md-7 col-xs-12" rows="5">{{ $user->alamat }}</textarea>
                        @if ($errors->has('alamat'))
                            <span class="help-block">
                      <strong>{{ $errors->first('alamat') }}</strong>
                  </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3 col-xs-12">Email</label>
                    <div class="col-sm-7 col-xs-12">
                        <input type="email" id="email" name="email" value="{{ $user->email }}" required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">
                        @if ($errors->has('email'))
                            <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3 col-xs-12">Telp</label>
                    <div class="col-sm-7 col-xs-12">
                        <input type="text" id="telp" name="telp" value="{{ $user->telp }}" required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">
                        @if ($errors->has('telp'))
                            <span class="help-block">
                      <strong>{{ $errors->first('telp') }}</strong>
                  </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3 col-xs-12">Foto Profil</label>
                    <div class="col-sm-5 col-xs-7">
                        <img src="{{ asset('storage/'.$user->foto_profil) }}" alt="image" height="75">
                        {{-- <input id="foto_profil" type="file" class="form-control" name="foto_profil">
                            @if ($errors->has('foto_profil'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('foto_profil') }}</strong>
                                </span>
                            @endif --}}
                    </div>
                </div> 

                <div class="form-group">
                    <label class="control-label col-sm-3 col-xs-12">Jabatan</label>
                    <div class="col-sm-3 col-sm-9">
                        <select class="form-control" name="jabatan" id="jabatan">
                            <option value="{{ $user->jabatan }}">- {{ $user->jabatan }} -</option>
                            <option value="superadmin">Super Admin</option>
                            <option value="admin">Admin</option>
                            <option value="owner">Owner</option>
                        </select>
                    </div>
                </div>
                <button class="btn btn-warning" type="masteruser">Cancel</button>
                <button class="btn btn-primary" type="submit">Update</button>
            </form>
        </div>
    </div>
@endsection