<script>
    $(document).ready(function (){
        var color = Chart.helpers.color;
        var diagrambarang = "/datatransaksi";
        var diagramuang = "/datauang";
        console.log(diagrambarang);


        $.get(diagrambarang, function (response) {
            var namabarang = new Array();
            var jumlah = new Array();

            response.forEach(function (data) {
                namabarang.push(data.nama_barang);
                jumlah.push(data.jumlah);
                // salesGrowth.push(data.sales_growth);
            });

            var ctx = document.getElementById("pieChart2").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: namabarang,
                    datasets: [{
                        backgroundColor: [
                            "#2ecc71",
                            "#3498db",
                            "#b1c914",
                            "#aa35c9",
                            "#c91441",
                        ],
                        data: jumlah
                    }]
                },
                options: {
                    legend: { display: false },
                }
            });
        });

        $.get(diagramuang, function (response) {
            var bulan = new Array();
            var total = new Array();

            response.forEach(function (data) {
                bulan.push(data.bulan);
                total.push(data.total);
                // salesGrowth.push(data.sales_growth);
            });

            var ctx = document.getElementById("mybarChart2").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: bulan,
                    datasets: [{
                        backgroundColor: [
                            "#c445cc",
                            "#3498db",
                            "#b1c914",
                            "#c91441",
                        ],
                        data: total
                    }]
                },
                options: {
                    legend: { display: false },
                }
            });
        });

    });
</script>