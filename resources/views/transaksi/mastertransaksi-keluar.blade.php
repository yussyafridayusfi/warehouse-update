@extends('layout.default')

@section('title')
    Master Transaksi
@endsection

@section('content')
    <div class="title_right">
        <h3><a href="{{route('transaksi-keluar.create')}}" class="btn-index btn btn-primary pull-right col-sm-2 col-sm-10" title="Add"><i class="glyphicon glyphicon-plus"></i></a></h3>
    </div>
    <div class="title_left">
        <h3>Master Transaksi</h3>
    </div>

    <div class="x_panel">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Master Data</a></li>
            <li class="breadcrumb-item active"><a href="">Transaksi Keluar</a></li>
        </ol>
        <div class="con">
            <table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="datatable-buttons_info" style="width: 1031px;">
                <thead>
                <tr>
                    <th scope="col">Nomor Transaksi</th>
                    <th scope="col">Customer</th>
                    <th scope="col">Barang</th>
                    <th scope="col">User</th>
                    <th scope="col">Jumlah</th>
                    <th scope="col">Harga</th>
                    <th scope="col">Total</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($transaksis_keluar as $transaksi)
                    {{-- @foreach(Auth::user()->User as $user) --}}
                <tr>
                    <td>{{ $transaksi->nomortransaksi }}</td>
                    <td>{{ $transaksi->nama_customer }}</td>
                    <td>{{ $transaksi->nama_barang }}</td>
                    <td>{{ $transaksi->nama_user }}</td>
                    <td>{{ $transaksi->jumlah }}</td>
                    <td>Rp. {{ $transaksi->harga }},-</td>
                    <td>Rp. {{ $transaksi->total }}</td>
                    <td>
                        <div id="lihat-modal-{{ $transaksi->id}}" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">
                                            <!-- Beri id "modal-title" untuk tag span pada judul modal -->
                                            <span id="modal-title">View Master User</span>
                                        </h4>
                                    </div>
                                    <form action="{{ route ('transaksi-keluar.show', $transaksi->id )}}" method="GET">
                                        <div class="modal-body" value="{{ $transaksi->id }}">
                                        @method('GET')
                                        @csrf
                                        <div class="form-group">
                                            <label>Nomor Transaksi </label>
                                            <div>
                                               <input type="text" class="form-control" id="nomortransaksi" name="nomortransaksi" value="{{ $transaksi->nomortransaksi }}" disabled="">  
                                            </div>
                                            
                                        </div>
        
                                        <div class="form-group">
                                            <label>Nama Barang </label>
                                            <div>
                                                <input type="text" class="form-control" id="nama_barang" name="nama_barang" value="{{ $transaksi->nama_barang }}" disabled=""> 
                                            </div>
                                            
                                        </div>
                                        <div class="form-group">
                                            <label>Customer</label>
                                            <div>
                                                <input type="text" class="form-control" id="nama_customer" name="nama_customer" value="{{ $transaksi->nama_customer }}" disabled="">
                                            </div>
                                            
                                        </div>
                                        <div class="form-group">
                                            <label>User</label>
                                            <div>
                                              <input type="text" class="form-control" id="nama_user" name="nama_user" value="{{ $transaksi->nama_user }}"disabled="" >  
                                            </div>
                                            
                                        </div>
                                        <div class="form-group">
                                            <label>Jumlah</label>
                                            <div>
                                                <input type="text" class="form-control" id="jumlah" name="jumlah" value="{{ $transaksi->jumlah }}" disabled="">
                                            </div>
                                            
                                        </div>
                                        <div class="form-group">
                                            <label>harga</label>
                                            <div>
                                               <input type="text" class="form-control" id="harga" name="harga" value="{{ $transaksi->harga }}" disabled=""> 
                                            </div>
                                            
                                        </div>
                                        <div class="form-group">
                                            <label>Date Created</label>
                                            <div>
                                                <input type="text" class="form-control" id="created_at" name="created_at" value="{{ $transaksi->created_at }}" disabled="">
                                            </div>
                                            
                                        </div>
                                        <div class="form-group">
                                            <label>Last Modifield</label>
                                            <div>
                                              <input type="text" class="form-control" id="updated_at" name="updated_at" value="{{ $transaksi->updated_at }}" disabled="">  
                                            </div>
                                            
                                        </div>


                                    </div>
                                </form>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="hapus-modal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">
                                            <!-- Beri id "modal-title" untuk tag span pada judul modal -->
                                            <span id="modal-title">Apakah anda ingin menghapus...?</span>
                                        </h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Ya</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#"  data-toggle="modal" data-target="#lihat-modal-{{ $transaksi->id  }}" class="btn btn-primary glyphicon glyphicon-eye-open" title="View"></a>

                        {{--<button class="btn btn-primary glyphicon glyphicon-eye-open" title="View" type="submit"></button>--}}
                        {{-- <a href="{{route('barang.edit',$barang->id)}}" class="btn btn-info glyphicon glyphicon-edit" title="Edit"></a> --}}
                       {{--  <button data-toggle="modal" data-target="#hapus-modal" class="btn btn-danger glyphicon glyphicon-trash" type="submit"></button> --}}

                    </td>
                </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection