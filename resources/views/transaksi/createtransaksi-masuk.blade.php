@extends('layout.default')

@section('title')
  Master Transaksi - Add
@endsection

@section('content')
  <div class="title_right">
    <h3><a href="{{route('transaksi-masuk.index')}}" class="btn-index btn btn-primary pull-right col-sm-2 col-sm-10" title="Back"><i class="fa fa-arrow-left"></i></a></h3>
  </div>
  <div class="title_left">
    <h3>Master Transaksi Masuk - Add</h3>
  </div>
  <div class="x_panel">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Master Data</a></li>
      <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
      <li class="breadcrumb-item active"><a href="#">Add</a></li>
    </ol>
    <div class="x_content">
      @if(session('alert'))
        <div class="alert {{ session('alert') == 'success' ? 'alert-success' : 'alert-danger' }}">
            <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>@if(session('alert') == 'success') Transaksi Barang Masuk Berhasil Ditambahkan. @else Transaksi Barang Masuk Gagal Ditambahkan. @endif</span>
        </div>
        @endif
        <form method="POST" action="{{route ('transaksi-masuk.store')}}" class="form-horizontal form-label-left" role="form" name="_token" enctype="multipart/form-data">
          @if($errors->isNotEmpty())
                    <div class="alert alert-warning" role="alert">
                        @foreach($errors->all() as $error)
                            <span>{{ $error }}</span>
                            <br>
                        @endforeach
                    </div>
                @endif
        {{csrf_field()}}
        
        <div class="form-group">
            <label class="control-label col-sm-3 col-xs-12">Nomor Transaksi</label>
            <div class="col-sm-3 col-xs-12">
                <input type="text" id="nomortransaksi" name="nomortransaksi" value="TransMsk{{$maxId}}" readonly="" required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">
                @if ($errors->has('nomortransaksi'))
                    <span class="help-block">
                    <strong>{{ $errors->first('nomortransaksi') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group">
          <label class="control-label col-sm-3 col-xs-12">Supplier</label>
          <div class="col-sm-3 col-sm-9">
            <select class="form-control" name="id_suplier" id="id_suplier">
              <option value="">-Nama Supplier-</option>
              @foreach($suppliers as $supplier)
              <option value="{{ $supplier->id }}">{{ $supplier->nama }}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-sm-3 col-xs-12">Barang</label>
          <div class="col-sm-3 col-sm-9">
            <select class="nama_barang form-control" name="id_barang" id="id_barang">
              <option value="">-Nama Barang-</option>
              @foreach($barangs as $barang)
              <option value="{{ $barang->id }}">{{ $barang->nama_barang }}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-sm-3 col-xs-12">User</label>
          <div class="col-sm-3 col-sm-9">
            <select class="form-control" name="id_user" id="id_user" >
              @foreach($users as $user)
              <option value="{{ $user->id }}">{{ $user->nama }}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-sm-3 col-xs-12">Jumlah</label>
          <div class="col-sm-3 col-xs-12">
              <input type="text" pattern="[0-9]+" title="only letters" id="jumlah" name="jumlah" value="{{old('jumlah')}}" required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">
              @if ($errors->has('jumlah'))
                  <span class="help-block">
                      <strong>{{ $errors->first('jumlah') }}</strong>
                  </span>
              @endif
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-3 col-xs-12">Harga</label>
          <div class="col-sm-3 col-xs-12">
              <input type="text" pattern="[0-9]+" title="only letters" id="harga" name="harga" value="{{old('harga')}}" required="required" class="hargaproduk form-control col-md-7 col-xs-12" value="" autofocus="">
              @if ($errors->has('harga'))
                  <span class="help-block">
                      <strong>{{ $errors->first('harga') }}</strong>
                  </span>
              @endif
          </div>
        </div> 

        <div class="form-group">
          <label class="control-label col-sm-3 col-xs-12">Harga Total</label>
          <div class="col-sm-7 col-xs-12">
              <input type="text" id="total" name="total" value=""  class="form-control col-md-7 col-xs-12" value="" autofocus="">
              @if ($errors->has('total'))
                  <span class="help-block">
                      <strong>{{ $errors->first('total') }}</strong>
                  </span>
              @endif
          </div>
        </div>        

        <button onclick="calc()" class="btn btn-primary" type="submit">Add</button>
      </form>
      
      
      </form>
    </div>
  </div>
@endsection

@section('script')

      <script type="text/javascript">
        $('#id_barang').on('change',e => {
              $.ajax({
                type:'get',
                url:'{!!URL::to('CariHarga')!!}',
                data:{'id':e.target.value},
                dataType:'json',
                success:function(data){
                  $('#harga').val(data)
                }
              });

          
        
          })

      </script>


      <script type="text/javascript">

        $(document).ready(function(){
            var jumlah=$("#jumlah");
            jumlah.keyup(function(){
                var total=isNaN(parseInt(jumlah.val()* $("#harga").val())) ? 0 :(jumlah.val()* $("#harga").val())
                $("#total").val(total);
            });

            $('#id_suplier').select2();
            $('#id_barang').select2();

        });
          
        </script>
@endsection