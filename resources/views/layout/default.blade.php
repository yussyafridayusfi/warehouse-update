<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    {{-- <img src="{{ asset('assets/images/img.jpg') }}" alt="..."> --}}
    <title>NKY-GUDANG | @yield('title') </title>

    <!-- Bootstrap -->
    <link href=" {{ asset ('assets/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href=" {{ asset ('assets/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- NProgress -->
    <link href=" {{ asset ('assets/vendors/nprogress/nprogress.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link href=" {{ asset ('assets/vendors/iCheck/skins/flat/green.css') }}" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href=" {{ asset ('assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet">
    <!-- JQVMap -->
    <link href=" {{ asset ('assets/vendors/jqvmap/dist/jqvmap.min.css') }}" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href=" {{ asset ('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

    <!-- bootstrap-datatable -->
    <link href=" {{ asset ('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.js') }}" rel="stylesheet">
    <link href=" {{ asset ('assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
    <link href=" {{ asset ('assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
    <link href=" {{ asset ('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <link href=" {{ asset ('assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
    <link href=" {{ asset ('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

    <!-- JQuery -->
    <link href=" {{ asset ('assets/vendors/jquery/dist/jquery.min.js') }}" rel="stylesheet">


    <!-- Custom Theme Style -->
    <link href=" {{ asset ('assets/css/custom.min.css') }}" rel="stylesheet">
    <link href=" {{ asset ('assets/vendors/jquery-chosen/chosen.min.css') }}" rel="stylesheet">
    <link href=" {{ asset ('assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="#" class="site_title"><i class="fa fa-home"></i><span> NKY-GUDANG</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                {{-- <img src="{{ asset('assets/images/img.jpg') }}" alt="..." class="img-circle profile_img"> --}}
                {{-- <td>
                        <img src="{{ asset('storage/'.$user->foto_profil) }}" alt="image" height="75">
                    </td> --}}
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                {{-- <h2>John Doe</h2> --}}
                <h1 style="text-align: center; -webkit-text-fill-color: white">{{Auth::User()->nama}}</h1>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  @if(Auth::User()->jabatan == 'owner'|Auth::User()->jabatan == 'superadmin')
                  <li><a href="{{route('dashboard.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard </a>
                  </li>
                  @endif
                  @if(Auth::User()->jabatan == 'superadmin'|Auth::User()->jabatan == 'owner')
                  <li><a><i class="fa fa-group"></i> Membership <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{route('user.index')}}">User</a></li>
                      <li><a href="{{route('supplier.index')}}">Supplier</a></li>
                      <li><a href="{{route('customer.index')}}">Customer</a></li>
                    </ul>
                  </li>
                  @endif
                  @if( Auth::User()->jabatan == 'superadmin'|Auth::User()->jabatan == 'admin'|Auth::User()->jabatan == 'owner')
                  <li><a><i class="fa fa-database"></i> Master Data <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{route('barang.index')}}">Barang</a></li>
                     <!-- <li><a href="transaksi">Transaksi</a></li> -->
                      <li><a> Transaksi <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="{{route('transaksi-masuk.index')}}">Masuk</a></li>
                          <li><a href="{{route('transaksi-keluar.index')}}">Keluar</a></li>
                        </ul>
                      </li>
                      <li><a> Retur <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="{{route('retur-masuk.index')}}">Masuk</a></li>
                          <li><a href="{{route('retur-keluar.index')}}">Keluar</a></li>
                        </ul>
                      </li>
                      <!--<li><a href="retur">Retur</a></li>-->
                      <li><a href="{{route('kategori.index')}}">Kategori</a></li>
                    </ul>
                  </li>
                  @endif
                  @if(Auth::User()->jabatan == 'superadmin'|Auth::User()->jabatan == 'owner')
                  <li><a><i class="fa fa-book"></i> Master Laporan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/laporan-membership">Membership</a></li>
                      <li><a href="/laporan-barang">Barang</a></li>
                      <li><a href="/laporan-transaksi">Transaksi</a></li>
                      <li><a href="/laporan-retur">Retur</a></li>
                    </ul>
                  </li>
                  @endif
                  @if(Auth::User()->jabatan == 'owner'|Auth::User()->jabatan == 'superadmin')
                  <li><a><i class="fa fa-history"></i> Logs Aktivitas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a> Logs Transaksi <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="/logs-masuk">Transaksi Masuk</a></li>
                          <li><a href="/logs-keluar">Transaksi Keluar</a></li>
                        </ul>
                      </li>
                      <li><a>Logs Retur <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="/logs-retur-masuk">Retur Masuk</a></li>
                          <li><a href="/logs-retur-keluar">Retur Keluar</a></li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                  @endif
                </ul>
              </div>
              <div class="menu_section">    
                  
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    {{-- <img src="images/img.jpg" alt=""> --}}
                    {{Auth::User()->nama}}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href=""></a></li>
                    <li><a href="/"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
        @yield('content')
        </div>
        <div class="clearfix"></div>
          <!-- footer content -->
              <footer>
                <div class="pull-right">
                  Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
                </div>
                <div class="clearfix"></div>
              </footer>
            <!-- /footer content -->
        </div>
      </div>

    
    <!-- jQuery -->
    <script src=" {{ asset ('assets/vendors/jquery/dist/jquery.min.js') }}"></script>
    <script src=" {{ asset ('assets/vendors/jquery-chosen/chosen.jquery.min.js') }}"></script>
    <script src=" {{ asset ('assets/vendors/select2/dist/js/select2.min.js') }}"></script>
    <script src=" {{ asset ('assets/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script src=" {{ asset ('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    
    <!-- Bootstrap -->
    <script src=" {{ asset ('assets/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- FastClick -->
    <script src=" {{ asset ('assets/vendors/fastclick/lib/fastclick.js') }}"></script>
    <!-- NProgress -->
    <script src=" {{ asset ('assets/vendors/nprogress/nprogress.js') }}"></script>
    <!-- Chart.js -->
    <script src=" {{ asset ('assets/vendors/Chart.js/dist/Chart.min.js') }}"></script>
    <!-- gauge.js -->
    <script src=" {{ asset ('assets/vendors/gauge.js/dist/gauge.min.js') }}"></script>
    <!-- bootstrap-progressbar -->
    <script src=" {{ asset ('assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
    <!-- iCheck -->
    <script src=" {{ asset ('assets/vendors/iCheck/icheck.min.js') }}"></script>
    <!-- Skycons -->
    <script src=" {{ asset ('assets/vendors/skycons/skycons.js') }}"></script>
    <!-- Flot -->
    <script src=" {{ asset ('assets/vendors/Flot/jquery.flot.js') }}"></script>
    <script src=" {{ asset ('assets/vendors/Flot/jquery.flot.pie.js') }}"></script>
    <script src=" {{ asset ('assets/vendors/Flot/jquery.flot.time.js') }}"></script>
    <script src=" {{ asset ('assets/vendors/Flot/jquery.flot.stack.js') }}"></script>
    <script src=" {{ asset ('assets/vendors/Flot/jquery.flot.resize.js') }}"></script>
    <!-- Flot plugins -->
    <script src=" {{ asset ('assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js') }}"></script>
    <script src=" {{ asset ('assets/vendors/flot-spline/js/jquery.flot.spline.min.js') }}"></script>
    <script src=" {{ asset ('assets/vendors/flot.curvedlines/curvedLines.js') }}"></script>
    <!-- DateJS -->
    <script src=" {{ asset ('assets/vendors/DateJS/build/date.js') }}"></script>
    <!-- JQVMap -->
    <script src=" {{ asset ('assets/vendors/jqvmap/dist/jquery.vmap.js') }}"></script>
    <script src=" {{ asset ('assets/vendors/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>
    <script src=" {{ asset ('assets/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js') }}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src=" {{ asset ('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src=" {{ asset ('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <!-- Custom Theme Scripts -->
    <script src=" {{ asset ('assets/js/custom.min.js') }}"></script>

    <!-- Datatables -->
    <script src="{{ asset ('assets/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset ('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset ('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset ('assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset ('assets/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset ('assets/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset ('assets/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset ('assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset ('assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset ('assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset ('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
    <script src="{{ asset ('assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset ('assets/vendors/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset ('assets/vendors/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset ('assets/vendors/pdfmake/build/vfs_fonts.js') }}"></script>
	
  @yield('script')

  </body>
</html>
