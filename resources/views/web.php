<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('/layout');
});*/
Route::get('/', function () {
    return view('login');
});
//Route::get('/viewprofile/{id}', 'profileController@view');
Route::get('/masterbarang', function () {
    return view('masterbarang');
});
Route::get('/dashboard', function () {
    return view('dashboard');
});
Route::get('/create', function () {
    return view('create');
});
Route::get('/editprofile', function () {
    return view('editprofile');
});
Route::get('/masteruserlevel', function () {
    return view('MasterUserlevel');
});
Route::get('/masteruser', function () {
    return view('masteruser');
});
Route::get('/createuser', function () {
    return view('createuser');
});
Route::get('/createuserlevel', function () {
    return view('createuserlevel');
});

Route::get('/indec', function () {
    return view('indec');
});
Route::get('/mastersupplier', function () {
    return view('mastersupplier');
});
Route::get('/mastersupplieradd', function () {
    return view('mastersupplieradd');
});
Route::get('/return', function () {
    return view('return');
});
Route::get('/returnamaster', function () {
    return view('returnamaster');
});
Route::get('/user', function () {
    return view('user');
});

Route::resource('layout','profileController');

