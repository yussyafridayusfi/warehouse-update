@extends('layout.webProfile')

@section('title')
    Web Profile - Notebook
@endsection 
          

@section('content')
        <h1 class="page-header">Notebook</h1>
        @foreach($kategori as $notebook)
          <div class="col-md-3">
            <br><br><br>
            <!--<h2 style="text-align: center;"> Notebook</h2>-->
              <div class="thumbnail">
                  <div class="card" style="width: 18rem;">
                      <img class="card-img-top" src="..." alt="{{$notebook->nama_barang}}">
                      <br>
                      <div class="card-body">
                        <div class="col">
                          <span class="help-block">
                            <img id="myImg" src="{{ asset('storage/'.$notebook->foto_barang) }}" alt="image" width="90%">
                          <span class="help-block">
                        </div>
                        <div class="col">
                          <div class="right">
                            <span class="help-block">
                            <a href="#" class="btn btn-primary" style="text-align: right;">STOK {{$notebook->jumlah}}</a>
                            <span class="help-block">
                          </div>
                        </div>
                      </div>
                  </div>
              </div>  
          </div>
       @endforeach 
      {{ $kategori->render() }} 
@endsection