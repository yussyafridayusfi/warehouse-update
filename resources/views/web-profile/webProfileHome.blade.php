@extends('layout.webProfile')

@section('title')
    Web Profile - Home
@endsection
          

@section('content')
        <h1 class="page-header">Barang Terbaru</h1>
        @foreach($newest as $new)
          <div class="col-md-3">
              <div class="thumbnail">
                  <div class="card" style="width: 18rem;">
                      <img class="card-img-top" src="..." alt="{{$new->nama_barang}}">
                      <br>
                      <div class="card-body">
                        <div class="col">
                          <span class="help-block">
                            <img id="myImg" src="{{ asset('storage/'.$new->foto_barang) }}" alt="image" width="90%">
                          </span>
                        </div>
                        <div class="col">
                          <div class="left">
                            <span class="help-block">
                              <a href="#" class="btn btn-primary" style="text-align: center;">STOK {{$new->jumlah}}</a>     
                            </span>
                          </div>
                        </div>   
                      </div>
                  </div>
              </div>
          </div>
        @endforeach 
        {{ $newest->render() }} 
@endsection