@extends('layout.webProfile')

@section('title')
    Web Profile - Laptop
@endsection
          

@section('content')
        <h1 class="page-header">Laptop</h1>
        @foreach($kategori as $laptop)
          <div class="col-md-3">
            <div class="thumbnail">
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="..." alt="{{$laptop->nama_barang}}">
                    <br>
                    <div class="card-body">
                      <div class="col">
                          <span class="help-block">
                            <img id="myImg" src="{{ asset('storage/'.$laptop->foto_barang) }}" alt="image" width="90%">
                          </span>
                      </div>
                      <div class="col">
                        <div class="right">
                          <span class="help-block">
                            <a href="#" class="btn btn-primary" style="text-align: right;">STOK {{$laptop->jumlah}}</a>
                          </span>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
       @endforeach 
      {{ $kategori->render() }} 
@endsection