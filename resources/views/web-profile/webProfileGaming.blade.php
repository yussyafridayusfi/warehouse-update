@extends('layout.webProfile')

@section('title')
    Web Profile - Laptop Gaming
@endsection
          

@section('content')
        <h1 class="page-header">Laptop - Gaming</h1>
        @foreach($kategori as $gaming)
          <div class="col-md-3">
              <div class="thumbnail">
                  <div class="card" style="width: 18rem;">
                      <img class="card-img-top" src="..." alt="{{$gaming->nama_barang}}">
                      <br>
                      <div class="card-body">
                        <div class="col">
                            <img id="myImg" src="{{ asset('storage/'.$gaming->foto_barang) }}" alt="image" width="90%">
                        </div>
                        <div class="col">
                          <div class="right">
                            <a href="#" class="btn btn-primary" style="text-align: right;">STOK {{$gaming->jumlah}}</a>
                          </div>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
        @endforeach 
        {{ $kategori->render() }} 
@endsection