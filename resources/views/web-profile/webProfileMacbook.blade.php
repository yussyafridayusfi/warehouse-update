@extends('layout.webProfile')

@section('title')
    Web Profile - Macbook
@endsection
          

@section('content')
        <h1 class="page-header">Macbook</h1>
        @foreach($kategori as $macbook)
          <div class="col-md-3">
              <div class="thumbnail">
                  <div class="card" style="width: 18rem;">
                      <img class="card-img-top" src="..." alt="{{$macbook->nama_barang}}">
                      <br>
                      <div class="card-body">
                        <div class="col">
                          <span class="help-block">
                            <img id="myImg" src="{{ asset('storage/'.$macbook->foto_barang) }}" alt="image" width="90%">
                          </span>
                        </div>
                        <div class="col">
                          <div class="right">
                            <span class="help-block">
                            <a href="#" class="btn btn-primary" style="text-align: right;">STOK {{$macbook->jumlah}}</a>
                            </span>
                          </div>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
       @endforeach 
      {{ $kategori->render() }}
@endsection