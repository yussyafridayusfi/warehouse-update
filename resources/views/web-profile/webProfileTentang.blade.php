@extends('layout.webProfile')

@section('title')
    Web Profile - Tentang Kami
@endsection
          

@section('content')
        <div class="col">
           <h1 class="page-header">Tentang Kami</h1>
            <h2>
              <p>
                Kami adalah perusahaan pergudangan di bidang elektronik, kami memiliki berbagai jenis laptop dan merk dengan harga yang terjangkau.
              </p> 
            <br>
            <p>
              Kunjungi Kami di 

              <div class="fa-hover col-md-3 col-sm-4 col-xs-12"><a href="#/maps.NKY-GUDANG"><i class="fa fa-map-marker"></i> Jl. Arif Rahman Hakim</a>
              </div>
              <div class="fa-hover col-md-3 col-sm-4 col-xs-12"><a href="#/gedung.NKY-GUDANG"><i class="fa fa-building"></i> Jl. Arif Rahman Hakim</a>
              </div>
              <br><br>
              <div class="fa-hover col-md-3 col-sm-4 col-xs-12"><a href="#/telp.NKY-GUDANG"><i class="fa fa-tty"></i> (031)5678905</a>
              </div>
              <div class="fa-hover col-md-3 col-sm-4 col-xs-12"><a href="#/hp.NKY-GUDANG"><i class="fa fa-mobile-phone"></i> 08978675645</a>
              </div>
              <br><br>
              <div class="fa-hover col-md-3 col-sm-4 col-xs-12"><a href="#/mail.NKY-GUDANG"><i class="fa fa-envelope"></i> NKY-GUDANG@gmail.com </a>
              </div>
            </p>
            <br><br><br>
            <p>
              Temukan Kami di 
              <div class="fa-hover col-md-3 col-sm-4 col-xs-12"><a href="#/facebook.NKY-GUDANG"><i class="fa fa-facebook"></i> facebook.NKY-GUDANG</a>
              </div>
              <br>
              <div class="fa-hover col-md-3 col-sm-4 col-xs-12"><a href="#/twitter.NKY-GUDANG"><i class="fa fa-twitter"></i> twitter.NKY-GUDANG</a>
              </div>
              <br>
              <div class="fa-hover col-md-3 col-sm-4 col-xs-12"><a href="#/instagram.NKY-GUDANG"><i class="fa fa-instagram"></i> instagram.NKY-GUDANG</a>
              </div>
            </p>
            </h2>
        </div>
@endsection