@extends('layout.default')

@section('title')
    Master Customer
@endsection

@section('content')
    <div class="title_right">
        <h3><a href="{{route('customer.create')}}" class="btn-index btn btn-primary pull-right col-sm-2 col-sm-10" title="Add"><i class="glyphicon glyphicon-plus"></i></a></h3>
    </div>
    <div class="title_left">
        <h3>Master Customer</h3>
    </div>

    <div class="x_panel">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Membership</a></li>
            <li class="breadcrumb-item active"><a href="">Customer</a></li>
        </ol>
        <div class="con">
            <table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="datatable-buttons_info" style="width: 1031px;">
                <thead>
                <tr>
                    <th scope="col">Nama</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Telp</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($customers as $customer)
                <tr>
                    <td>{{ $customer->nama }}</td>       
                    <td>{{ $customer->alamat }}</td>
                    <td>{{ $customer->telp }}</td>
                    <td>
                        <div id="lihat-modal-{{ $customer->id}}" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">
                                            <span id="modal-title">View Master Customer</span>
                                        </h4>
                                    </div> 
                                    <form action="{{ route ('customer.show', $customer->id )}}" method="GET">
                                        <div class="modal-body" value="{{ $customer->id }}">
                                        @method('GET')
                                            @csrf
                                        <div class="form-group">
                                            <label>ID</label>
                                            <div>
                                                <input type="text" class="form-control" id="id" name="id" value="{{ $customer->id }}" disabled="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama</label>
                                            <div>
                                                <input type="text" class="form-control" id="nama" name="nama" value="{{ $customer->nama }}" disabled="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat</label>
                                            <div>
                                                <input type="text" class="form-control" id="alamat" name="alamat" value="{{ $customer->alamat }}" disabled="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Telp</label>
                                            <div>
                                                <input type="text" class="form-control" id="telp" name="telp" value="{{ $customer->telp }}"disabled="" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Date Created</label>
                                            <div>
                                                <input type="text" class="form-control" id="created_at" name="created_at" value="{{ $customer->created_at }}" disabled="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Last Modifield</label>
                                            <div>
                                                <input type="text" class="form-control" id="updated_at" name="updated_at" value="{{ $customer->updated_at }}" disabled="">
                                            </div>
                                        </div>

                                    </div>
                                    </form>
                                    
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="hapus-modal-{{ $customer->id  }}" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">
                                            <!-- Beri id "modal-title" untuk tag span pada judul modal -->
                                            <span id="modal-title">Apakah anda ingin menghapus...?</span>
                                        </h4>
                                    </div>
                                    <div class="modal-footer">
                                        <form action="{{ route ('customer.destroy', $customer->id )}}" method="post">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-default">Ya</button>
                                        </form>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#"  data-toggle="modal" data-target="#lihat-modal-{{ $customer->id}}" class="btn btn-primary glyphicon glyphicon-eye-open" title="View"></a>

                        {{--<button class="btn btn-primary glyphicon glyphicon-eye-open" title="View" type="submit"></button>--}}
                        <a href="{{route('customer.edit',$customer->id)}}" class="btn btn-info glyphicon glyphicon-edit" title="Edit"></a>
                        <button data-toggle="modal" data-target="#hapus-modal-{{ $customer->id  }}" class="btn btn-danger glyphicon glyphicon-trash"></button>
                    </td>
                </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
@endsection