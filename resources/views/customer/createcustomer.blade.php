@extends('layout.default')

@section('title')
    Master Customer - Add
@endsection

@section('content')
    <div class="title_right">
        <h3><a href="{{route('customer.index')}}" class="btn-index btn btn-primary pull-right col-sm-2 col-sm-10" title="Back"><i class="fa fa-arrow-left"></i></a></h3>
    </div>
    <div class="title_left">
        <h3>Master Customer - Add</h3>
    </div>

    <div class="x_panel">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Membership</a></li>
            <li class="breadcrumb-item"><a href="#">Customer</a></li>
            <li class="breadcrumb-item"><a href="">Add</a></li>
        </ol>
        @if(session('alert'))
        <div class="alert {{ session('alert') == 'success' ? 'alert-success' : 'alert-danger' }}">
            <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>@if(session('alert') == 'success') Customer Berhasil Ditambahkan. @else Customer Gagal Ditambahkan. @endif</span>
        </div>
        @endif
        <div class="x_content">
            <form method="post" action="{{route ('customer.store')}}" class="form-horizontal form-label-left" role="form"  name="_token" enctype="multipart/form-data">
                @if($errors->isNotEmpty())
                    <div class="alert alert-warning" role="alert">
                        @foreach($errors->all() as $error)
                            <span>{{ $error }}</span>
                            <br>
                        @endforeach
                    </div>
                @endif
                {{csrf_field()}}

                <div class="form-group">
                    <label class="control-label col-sm-3 col-xs-12">Nama</label>
                    <div class="col-sm-5 col-xs-12">
                        <input type="text" id="nama" name="nama" value="{{old('nama')}}" required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">
                        @if ($errors->has('nama'))
                            <span class="help-block">
                                <strong>{{ $errors->first('nama') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3 col-xs-12">Alamat</label>
                    <div class="col-sm-5 col-xs-12">
                        <input type="text" id="alamat" name="alamat" value="{{old('alamat')}}" required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">
                        @if ($errors->has('alamat'))
                            <span class="help-block">
                                <strong>{{ $errors->first('alamat') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3 col-xs-12">Telp</label>
                        <div class="col-sm-5 col-sm-9">
                            <input type="tel" class="form-control" data-inputmask="'mask' : '(999) 999-999-999'" id="telp" name="telp" value="{{old('telp')}}" required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">
                            @if ($errors->has('telp'))
                                <span class="help-block">
                                <strong>{{ $errors->first('telp') }}</strong>
                            </span>
                            @endif
                            <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                    </div>
                <button class="btn btn-primary" type="submit">Add</button>
            </form>
        </div>
    </div>
@endsection