@extends('layout.default')

@section('title')
  Master Barang - Add
@endsection

@section('content')
  <div class="title_right">
    <h3><a href="{{route('barang.index')}}" class="btn-index btn btn-primary pull-right col-sm-2 col-sm-10" title="Back"><i class="fa fa-arrow-left"></i></a></h3>
  </div>
  <div class="title_left">
    <h3>Master Barang - Add</h3>
  </div>
  <div class="x_panel">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Master Data</a></li>
      <li class="breadcrumb-item"><a href="#">Barang</a></li>
      <li class="breadcrumb-item"><a href="#">Add</a></li>
    </ol>
    <div class="x_content">
      @if(session('alert'))
        <div class="alert {{ session('alert') == 'success' ? 'alert-success' : 'alert-danger' }}">
            <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>@if(session('alert') == 'success') Barang Berhasil Ditambahkan. @else Barang Gagal Ditambahkan. @endif</span>
        </div>
        @endif
        <form method="post" action="{{route ('barang.store')}}" class="form-horizontal form-label-left" role="form" name="_token" enctype="multipart/form-data">
           @if($errors->isNotEmpty())
                    <div class="alert alert-warning" role="alert">
                        @foreach($errors->all() as $error)
                            <span>{{ $error }}</span>
                            <br>
                        @endforeach
                    </div>
                @endif
        {{csrf_field()}}

        <div class="form-group">
            <label class="control-label col-sm-3 col-xs-12">Nama Barang</label>
            <div class="col-sm-5 col-xs-12">
                <input type="text" id="nama_barang" name="nama_barang" value="{{old('nama_barang')}}" required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">
                @if ($errors->has('nama_barang'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nama_barang') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3 col-xs-12">Kode Barang</label>
            <div class="col-sm-5 col-xs-12">
                <input type="text" id="kode_barang" name="kode_barang" value="Brg{{$maxId}}" readonly="" class="form-control col-md-7 col-xs-12" rows="5"></input>
                @if ($errors->has('kode_barang'))
                  <span class="help-block">
                      <strong>{{ $errors->first('kode_barang') }}</strong>
                  </span>
              @endif
            </div>
        </div>

        <div class="form-group">
          <label class="control-label col-sm-3 col-xs-12">Kategori</label>
          <div class="col-sm-3 col-sm-9">
            <select class="form-control" name="id_kategori" id="id_kategori">
              @foreach($kategoris as $kategori)
              <option value="{{ $kategori->id }}">{{ $kategori->nama_kategori }}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-sm-3 col-xs-12">Jumlah</label>
          <div class="col-sm-2 col-xs-6">
              <input type="number" pattern="[0-9]+" title="only numbers" id="jumlah" name="jumlah" value="{{old('jumlah')}}" required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">
              @if ($errors->has('jumlah'))
                  <span class="help-block">
                      <strong>{{ $errors->first('jumlah') }}</strong>
                  </span>
              @endif
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-3 col-xs-12">Harga</label>
          <div class="col-sm-2 col-xs-6">
              <input type="number" pattern="[0-9]+" title="only letters" id="harga" name="harga" value="{{old('harga')}}" required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">
              @if ($errors->has('harga'))
                  <span class="help-block">
                      <strong>{{ $errors->first('harga') }}</strong>
                  </span>
              @endif
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-sm-3 col-xs-12">Deskripsi</label>
          <div class="col-sm-5 col-xs-12">
              <textarea type="text" id="deskripsi" name="deskripsi" value="{{old('deskripsi')}}" required="required" class="form-control col-md-7 col-xs-12" value="" autofocus=""></textarea>
              @if ($errors->has('deskripsi'))
                  <span class="help-block">
                      <strong>{{ $errors->first('deskripsi') }}</strong>
                  </span>
              @endif
          </div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-sm-3 col-xs-12">Foto Barang</label>
            <div class="col-sm-5 col-xs-7">
                <input id="foto_barang" type="file" class="form-control" name="foto_barang" onchange="validate(this.value)">
                
                @if ($errors->has('foto_barang'))
                    <span class="help-block">
                        <strong>{{ $errors->first('foto_barang') }}</strong>
                    </span>
                @endif
            </div>
           <!-- <p class="err" style="color: red"></p>-->
        </div>
        <div class="form-group">
          <label class="control-label col-sm-3 col-xs-12"></label>
          <div class="col-sm-5 col-xs-7">
            <p class="err" style="color: red"></p>
          </div>
          
        </div>
        
        <button class="btn btn-primary" type="submit">Add</button>
      </form>
    </div>
  </div>
@endsection
@section('script')
  <script type="text/javascript">
    function validate(file){
      var ext = file.substr(file.lastIndexOf('.')+1);
      var allow=["jpg","jpeg",'png','bmp'];
      if(allow.lastIndexOf(ext)==-1){
        $(".err").html('ERROR! The File Must Be Image!');
      }
    }
  </script>


    <script type="text/javascript">
        $('#id_kategori').select2();
    </script>
@endsection