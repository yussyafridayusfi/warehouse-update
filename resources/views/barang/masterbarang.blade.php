@extends('layout.default')

@section('title')
    Master Barang
@endsection

@section('content')
    <div class="title_right">
        <h3><a href="{{route('barang.create')}}" class="btn-index btn btn-primary pull-right col-sm-2 col-sm-10" title="Add"><i class="glyphicon glyphicon-plus"></i></a></h3>
    </div>
    <div class="title_left">
        <h3>Master Barang</h3>
    </div>

    <div class="x_panel">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Master Data</a></li>
            <li class="breadcrumb-item active"><a href="#">Barang</a></li>
        </ol>
        <div class="con">
            <table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="datatable-buttons_info" style="width: 1031px;">
                <thead>
                <tr>
                    <th scope="col">Nama Barang</th>
                    <th scope="col">Kode Barang</th>
                    <th scope="col">Kategori</th>
                    <th scope="col">Jumlah</th>
                    <th scope="col">Harga</th>
                    <th scope="col">Gambar</th>
                    <th scope="col">Deskripsi</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($barangs as $barang)
                <tr>
                    <td>{{ $barang->nama_barang }}</td>       
                    <td>{{ $barang->kode_barang }}</td>
                    <td>{{ $barang->nama_kategori }}</td>
                    <td>{{ $barang->jumlah }}</td>
                    <td>{{ $barang->harga }}</td>
                    <td>
                        <img src="{{ asset('storage/'.$barang->foto_barang) }}" alt="image" height="75">
                    </td>
                    <td>{{ $barang->deskripsi }}</td>
                    <td>
                        <div id="lihat-modal-{{ $barang->id}}" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">
                                            <span id="modal-title">View Master Barang</span>
                                        </h4>
                                    </div>
                                   <form action="{{ route ('barang.show', $barang->id )}}" method="GET">
                                        <div class="modal-body" value="{{ $barang->id }}">
                                        @method('GET')
                                        @csrf
                                            <div class="form-group">
                                                <label>Nama Barang </label>
                                                <div>
                                                    <input type="text" class="form-control" id="nama_barang" name="nama_barang" value="{{ $barang->nama_barang }}" disabled="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Kode Barang </label>
                                                <div>
                                                    <input type="text" class="form-control" id="kode_barang" name="kode_barang" value="{{ $barang->kode_barang }}" disabled="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Kategori</label>
                                                <div>
                                                    <input type="text" class="form-control" id="nama_kategori" name="id_kategori" value="{{ $barang->nama_kategori }}" disabled="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Jumlah</label>
                                                <div>
                                                    <input type="text" class="form-control" id="jumlah" name="jumlah" value="{{ $barang->jumlah }}" disabled="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Harga</label>
                                                <div>
                                                    <input type="text" class="form-control" id="harga" name="harga" value="{{ $barang->harga }}" disabled="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Deskripsi</label>
                                                <div>
                                                    <input type="text" class="form-control" id="deskripsi" name="deskripsi" value="{{ $barang->deskripsi }}" disabled="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Date Created</label>
                                                <div>
                                                    <input type="text" class="form-control" id="created_at" name="created_at" value="{{ $barang->created_at }}" disabled="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Last Modifield</label>
                                                <div>
                                                    <input type="text" class="form-control" id="updated_at" name="updated_at" value="{{ $barang->updated_at }}" disabled="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Foto Barang</label><br>
                                                <div>
                                                    <img src="{{ asset('storage/'.$barang->foto_barang) }}" alt="image" height="128">
                                                </div>
                                            </div>

                                        </div> 
                                    </form>
                                    
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="hapus-modal-{{ $barang->id  }}" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">
                                            <!-- Beri id "modal-title" untuk tag span pada judul modal -->
                                            <span id="modal-title">Apakah anda ingin menghapus <strong>{{ $barang->nama_barang }}</strong>?</span>
                                        </h4>
                                    </div>
                                    <div class="modal-footer">
                                        <form action="{{ route ('barang.destroy', $barang->id )}}" method="post">
                                            @method('DELETE')
                                            @csrf
                                           <!-- <input type="submit" class="btn btn-default">Ya-->
                                            <button type="submit" class="btn btn-default" >Ya</button>
                                        </form>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#"  data-toggle="modal" data-target="#lihat-modal-{{ $barang->id  }}" class="btn btn-primary glyphicon glyphicon-eye-open" title="View"></a>
                        <a href="{{route('barang.edit',$barang->id)}}" class="btn btn-info glyphicon glyphicon-edit" title="Edit"></a>
                        <button data-toggle="modal" data-target="#hapus-modal-{{ $barang->id  }}" class="btn btn-danger glyphicon glyphicon-trash" ></button>

                    </td>
                </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection