@extends('layout.default')

@section('title')
    Master User - Edit
@endsection

@section('content')
    <div class="title_right">
        <h3><a href="{{route('barang.index')}}" class="btn-index btn btn-primary pull-right col-sm-2 col-sm-10" title="Back"><i class="fa fa-arrow-left"></i></a></h3>
    </div>
    <div class="title_left">
        <h3>Master User - Edit</h3>
    </div>
    <div class="x_panel">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Master Data</a></li>
            <li class="breadcrumb-item"><a href="#">Barang</a></li>
            <li class="breadcrumb-item"><a href="#">Edit</a></li>
        </ol>
        <div class="x_content">
            @if(session('alert'))
                <div class="alert {{ session('alert') == 'success' ? 'alert-success' : 'alert-danger' }}">
                    <span>@if(session('alert') == 'success') Barang Berhasil Diedit. @else Barang Gagal Diedit. @endif</span>
                </div>
            @endif

            <form method="post" action="{{route ('barang.update', $barangs->id)}}" class="form-horizontal form-label-left" role="form" name="_token" enctype="multipart/form-data">
                {{method_field('PUT')}}

                {{csrf_field()}}

                <div class="form-group">
                    <label class="control-label col-sm-3 col-xs-12">Nama Barang</label>
                    <div class="col-sm-5 col-xs-12">
                        <input type="text" id="nama_barang" name="nama_barang" value="{{ $barangs->nama_barang }}" required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">
                        @if ($errors->has('nama_barang'))
                            <span class="help-block">
                        <strong>{{ $errors->first('nama_barang') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3 col-xs-12">Kode Barang</label>
                    <div class="col-sm-3 col-xs-6">
                        <input type="text" id="kode_barang" name="kode_barang" value="{{ $barangs->kode_barang }}" required="required" class="form-control col-md-7 col-xs-12" autofocus="" >
                        <!--<textarea id="kode_barang" name="kode_barang" class="form-control col-md-7 col-xs-12" rows="5">{{ $barangs->kode_barang }}</textarea>-->
                        @if ($errors->has('kode_barang'))
                            <span class="help-block">
                      <strong>{{ $errors->first('kode_barang') }}</strong>
                  </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3 col-xs-12">Kategori</label>
                    <div class="col-sm-3 col-sm-9">
                        <select class="form-control" name="id_kategori" id="id_kategori">
                            @foreach($kategoris as $kategori)
                                @if ($kategori->id == $barangs->id_kategori)
                                    <option value="{{ $barangs->id_kategori }}" selected="selected">{{ $kategori->nama_kategori }}</option>
                                @else
                                    <option value="{{ $kategori->id }}">{{ $kategori->nama_kategori }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>

               {{--  <div class="form-group">
                    <label class="control-label col-sm-3 col-xs-12">Supplier</label>
                    <div class="col-sm-3 col-sm-9">
                        <select class="form-control" name="id_suplier" id="id_suplier">
                            @foreach($suppliers as $supplier)
                                @if ($supplier->id == $barangs->id_suplier)
                                    <option value="{{ $barangs->id_suplier }}" selected="selected">{{ $supplier->nama }}</option>
                                @else
                                    <option value="{{ $supplier->id }}">{{ $supplier->nama }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div> --}}

                {{-- <div class="form-group">
                    <label class="control-label col-sm-3 col-xs-12">User</label>
                    <div class="col-sm-3 col-sm-9">
                        <select class="form-control" name="id_user" id="id_user">
                            @foreach($users as $user)
                                @if ($user->id == $barangs->id_user)
                                    <option value="{{ $barangs->id_user }}" selected="selected">{{ $user->nama }}</option>
                                @else
                                    <option value="{{ $user->id}}">{{ $user->nama }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div> --}}

                <div class="form-group">
                    <label class="control-label col-sm-3 col-xs-12">Jumlah</label>
                    <div class="col-sm-2 col-xs-6">
                        <input type="number" id="jumlah" name="jumlah" value="{{ $barangs->jumlah }}" required="required" class="form-control col-sm-2 col-xs-6" value="" autofocus="" min="1" max="100">
                        <input type="hidden" id="jumlah" name="jml" value="{{ $barangs->jumlah }}">
                        @if ($errors->has('jumlah'))
                            <span class="help-block">
                      <strong>{{ $errors->first('jumlah') }}</strong>
                  </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3 col-xs-12">Harga</label>
                    <div class="col-sm-2 col-xs-6">
                        <input type="text" id="harga" name="harga" value="{{ $barangs->harga }}" required="required" class="form-control col-sm-2 col-xs-6" value="" autofocus="">
                        @if ($errors->has('harga'))
                            <span class="help-block">
                      <strong>{{ $errors->first('harga') }}</strong>
                  </span>
                        @endif
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-sm-3 col-xs-12">Deskripsi</label>
                    <div class="col-sm-5 col-xs-12">
                        <textarea type="text" id="deskripsi" name="deskripsi"  required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">{{ $barangs->deskripsi }}</textarea>
                        <!--<input type="text" id="deskripsi" name="deskripsi" value="{{ $barangs->deskripsi }}" required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">-->
                        @if ($errors->has('deskripsi'))
                            <span class="help-block">
                      <strong>{{ $errors->first('deskripsi') }}</strong>
                  </span>
                        @endif
                    </div>
                </div>

                {{-- <div class="form-group">
                    <label class="control-label col-sm-3 col-xs-12">Foto Barang</label>
                    <div class="col-sm-5 col-xs-7">
                        <img src="{{ asset('storage/'.$barangs->foto_barang) }}" alt="image" height="75">
                        <input id="foto_barang" type="file" class="form-control" name="foto_barang" value="{{ $barangs->foto_barang }}">
                        @if ($errors->has('foto_barang'))
                            <span class="help-block">
                        <strong>{{ $errors->first('foto_barang') }}</strong>
                    </span>
                        @endif
                    </div>
                </div> --}}
                <button class="btn btn-warning" type="/barang">Cancel</button>
                <button class="btn btn-primary" type="submit">Update</button>
            </form>
        </div>
    </div>
@endsection