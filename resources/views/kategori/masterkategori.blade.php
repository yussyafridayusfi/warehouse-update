@extends('layout.default')

@section('title')
    Master Kategori
@endsection

@section('content')
    <div class="title_right">
        <h3><a href="{{route('kategori.create')}}" class="btn-index btn btn-primary pull-right col-sm-2 col-sm-10" title="Add"><i class="glyphicon glyphicon-plus"></i></a></h3>
    </div>
    <div class="title_left">
        <h3>Master Kategori</h3>
    </div>

    <div class="x_panel">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Master Data</a></li>
            <li class="breadcrumb-item active"><a href="">Kategori</a></li>
        </ol>
        <div class="con">
            <table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="datatable-buttons_info" style="width: 1031px;">
                <thead>
                <tr>
                    <th scope="col">Nama Kategori</th>
                    <th scope="col">Total</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($kategoris as $kategori)
                    {{-- @foreach(Auth::user()->User as $user) --}}
                <tr>
                    <td>{{ $kategori->nama_kategori }}</td>
                    <td></td>
                    <td>
                        <div id="lihat-modal-{{ $kategori->id}}" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">
                                            <!-- Beri id "modal-title" untuk tag span pada judul modal -->
                                            <span id="modal-title">View Master Kategori</span>
                                        </h4>
                                    </div>
                                    <form action="{{ route ('kategori.show', $kategori->id )}}" method="GET">
                                        <div class="modal-body" value="{{ $kategori->id }}">
                                        @method('GET')
                                        @csrf
                                        <div class="form-group">
                                            <label>ID</label>
                                            <div>
                                                <input type="text" class="form-control" id="id" name="id" value="{{ $kategori->id }}" disabled="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Kategori</label>
                                            <div>
                                                <input type="text" class="form-control" id="nama" name="nama" value="{{ $kategori->nama_kategori }}" disabled="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                                <label>Date Created</label>
                                            <div>
                                                <input type="text" class="form-control" id="created_at" name="created_at" value="{{ $kategori->created_at }}" disabled="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Last Modifield</label>
                                            <div>
                                                <input type="text" class="form-control" id="updated_at" name="updated_at" value="{{ $kategori->updated_at }}" disabled="">
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                    
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="hapus-modal-{{ $kategori->id  }}" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">
                                            <!-- Beri id "modal-title" untuk tag span pada judul modal -->
                                            <span id="modal-title">Apakah anda ingin menghapus...?</span>
                                        </h4>
                                    </div>
                                    <div class="modal-footer">
                                        <form action="{{ route ('kategori.destroy', $kategori->id )}}" method="post">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-default">Ya</button>
                                        </form>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#"  data-toggle="modal" data-target="#lihat-modal-{{ $kategori->id}}" class="btn btn-primary glyphicon glyphicon-eye-open" title="View"></a>
                        <a href="{{route('kategori.edit',$kategori->id)}}" class="btn btn-info glyphicon glyphicon-edit" title="Edit"></a>
                        <button data-toggle="modal" data-target="#hapus-modal-{{ $kategori->id  }}" class="btn btn-danger glyphicon glyphicon-trash" type="submit"></button>
                    </td>
                </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection