@extends('layout.default')

@section('title')
    Master Kategori - Add
@endsection

@section('content')
    <div class="title_right">
        <h3><a href="{{route('kategori.index')}}" class="btn-index btn btn-primary pull-right col-sm-2 col-sm-10" title="Back"><i class="fa fa-arrow-left"></i></a></h3>
    </div>
    <div class="title_left">
        <h3>Master Kategori - Add</h3>
    </div>

    <div class="x_panel">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Master Data</a></li>
            <li class="breadcrumb-item"><a href="#">Kategori</a></li>
            <li class="breadcrumb-item"><a href="">Add</a></li>
        </ol>
        @if(session('alert'))
        <div class="alert {{ session('alert') == 'success' ? 'alert-success' : 'alert-danger' }}">
            <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>@if(session('alert') == 'success') Kategori Berhasil Ditambahkan. @else Customer Gagal Ditambahkan. @endif</span>
        </div>
        @endif
        <div class="x_content">
            <form method="post" action="{{route ('kategori.store')}}" class="form-horizontal form-label-left" role="form"  name="_token" enctype="multipart/form-data">
                @if($errors->isNotEmpty())
                    <div class="alert alert-warning" role="alert">
                        @foreach($errors->all() as $error)
                            <span>{{ $error }}</span>
                            <br>
                        @endforeach
                    </div>
                @endif
                {{csrf_field()}}

                <div class="form-group">
                    <label class="control-label col-sm-3 col-xs-12">Nama Kategori</label>
                    <div class="col-sm-5 col-xs-12">
                        <input type="text" id="nama_kategori" name="nama_kategori" value="{{old('nama_kategori')}}" required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">
                        @if ($errors->has('nama_kategori'))
                            <span class="help-block">
                                <strong>{{ $errors->first('nama_kategori') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                
                <button class="btn btn-primary" type="submit">Add</button>
            </form>
        </div>
    </div>
@endsection