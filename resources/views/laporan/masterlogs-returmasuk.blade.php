@extends('layout.default')

@section('title')
    Master Logs
@endsection

@section('content')
    <div class="title_right">
        <h3><a href="/logs-retur-masuk" class="btn-index btn btn-primary pull-right col-sm-2 col-sm-10" title="refresh"><i class="glyphicon glyphicon-refresh"></i></a></h3>
    </div>
    <div class="title_left">
        <h3>Master Logs <small>Retur Masuk</small></h3>
    </div>
    <div class="x_panel">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Logs Aktifitas</a></li>
            <li class="breadcrumb-item"><a href="#">Logs Retur Masuk</a></li>
        </ol>
        <div class="x_content">
            <form method="post" action="{{route('getDataLapRMasuk')}}" class="form-horizontal form-label-left" role="form">
                {{csrf_field()}}
                <div class="form-group">
                    <div class="col-md-4">
                        <h2>Date Report</h2>
                        <form class="form-horizontal">
                          <fieldset>
                            <div class="control-group">
                              <div class="controls">
                                <div class="input-prepend input-group">
                                  <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                  <input type="text" style="width: 200px" name="reservation" id="reservation" class="form-control" value="" />
                                </div>
                              </div>
                            </div>
                          </fieldset>
                        </form>
                      </div>
                </div>

                <div class="con">
                    <table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="datatable-buttons_info" style="width: 1031px;">
                        <thead>
                        <tr>
                            <th scope="col">Supplier</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">User</th>
                            <th scope="col">Stok Lama</th>
                            <th scope="col">Stok Baru</th>
                            <th scope="col">Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($logs as $log)
                        <tr>     
                            <td>{{ $log->nama_target }}</td>
                            <td>{{ $log->nama_barang }}</td>
                            <td>{{ $log->user }}</td>
                            <td>{{ $log->stok_lama }}</td>
                            <td>{{ $log->stok_baru }}</td>
                            <td>{{ $log->keterangan }}</td>
                        </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">

        $('#reservation').on('change',e => {
              $.ajax({
                type:'post',
                url:'{!!URL::to('getDataLapRMasuk')!!}',
                data:{'created_at':e.target.value},
                dataType:'json',
                success:function(data){
                  $('#datatable-buttons').val(data)
                }
              });
        
          })

      </script>
@endsection