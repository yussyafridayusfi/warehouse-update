@extends('layout.default')

@section('title')
    Master Laporan - Add
@endsection

@section('content')
    <div class="title_right">
        <h3><a href="/laporan-retur" class="btn-index btn btn-primary pull-right col-sm-2 col-sm-10" title="refresh"><i class="glyphicon glyphicon-refresh"></i></a></h3>
    </div>
    <div class="title_left">
        <h3>Master Laporan - Retur</h3>
    </div>
    <div class="x_panel">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Master Laporan</a></li>
            <li class="breadcrumb-item"><a href="#">Laporan</a></li>
            <li class="breadcrumb-item active"><a href=""></a>Add</li>
        </ol>
        <div class="x_content">
            <form method="post" action="{{route('getDataRetur')}}" class="form-horizontal form-label-left" role="form">
                {{csrf_field()}}
                <div class="form-group">
                    <div class="col-md-4">
                        <h4>Date Report</h4>
                        <form class="form-horizontal">
                          <fieldset>
                            <div class="control-group">
                              <div class="controls">
                                <div class="input-prepend input-group">
                                  <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                  <input type="text" style="width: 200px" name="reservation" id="reservation" class="form-control" value="" />
                                </div>
                              </div>
                            </div>
                          </fieldset>
                        </form>
                    </div>
                </div>

                <div class="con">
                    <table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="datatable-buttons_info" style="width: 1031px;">
                        <thead>
                        <tr>
                            <th scope="col">Nomor Retur</th>
                            <th scope="col">Jenis Retur</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Jumlah</th>
                            <th scope="col">Keterangan</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($returs as $retur)
                        <tr>
                            <td>{{ $retur->nomortransaksi }}</td>
                            <td>{{ $retur->jenisretur }}</td>
                            <td>{{ $retur->nama_barang }}</td>
                            <td>{{ $retur->jumlah }}</td>
                            <td>{{ $retur->keterangan }}</td>
                        
                        </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">

        $('#reservation').on('change',e => {
              $.ajax({
                type:'post',
                url:'{!!URL::to('getDataRetur')!!}',
                data:{'created_at':e.target.value},
                dataType:'json',
                success:function(data){
                  $('#datatable-buttons').val(data)
                }
              });
        
          })

    </script>
@endsection