@extends('layout.default')

@section('title')
    Master retur
@endsection

@section('content')
    <div class="title_right">
        <h3><a href="{{route('retur-masuk.create')}}" class="btn-index btn btn-primary pull-right col-sm-2 col-sm-10" title="Add"><i class="glyphicon glyphicon-plus"></i></a></h3>
    </div>
    <div class="title_left">
        <h3>Master Retur Masuk</h3>
    </div>

    <div class="x_panel">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Master Data</a></li>
            <li class="breadcrumb-item active"><a href="">Retur Masuk</a></li>
        </ol>
        <div class="con">
            <table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer dtr-inline collapsed" role="grid" aria-describedby="datatable-buttons_info" style="width: 1031px;">
                <thead>
                <tr>
                    <th scope="col">Nomor Transaksi</th>
                    <th scope="col">Nama Barang</th>
                    <th scope="col">Jumlah</th>
                    <th scope="col">Keterangan</th>
                    <th scope="col">Supplier</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($returs as $retur)
                <tr>
                    <td>{{ $retur->nomortransaksi }}</td>
                    <td>{{ $retur->nama_barang }}</td>
                    <td>{{ $retur->jumlah }}</td>
                    <td>{{ $retur->keterangan }}</td>
                    <td>{{ $retur->nama_target }}</td>
                    <td>
                        <div id="lihat-modal-{{ $retur->id}}" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">
                                            <!-- Beri id "modal-title" untuk tag span pada judul modal -->
                                            <span id="modal-title">View Master Supplier</span>
                                        </h4>
                                    </div>
                                    <form action="{{ route ('retur-masuk.show', $retur->id )}}" method="GET">
                                        <div class="modal-body" value="{{ $retur->id }}">
                                        @method('GET')
                                        @csrf
                                        <div class="form-group">
                                            <label>ID</label>
                                            <div>
                                                <input type="text" class="form-control" id="id" name="id" value="{{ $retur->id }}" disabled="">   
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Nomor Transaksi</label>
                                            <div>
                                                <input type="text" class="form-control" id="nomortransaksi" name="nomortransaksi" value="{{ $retur->nomortransaksi }}" disabled="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Barang</label>
                                            <div>
                                                <input type="text" class="form-control" id="nama_barang" name="nama_barang" value="{{ $retur->nama_barang }}" disabled="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Jumlah</label>
                                            <div>
                                                <input type="text" class="form-control" id="jumlah" name="jumlah" value="{{ $retur->jumlah }}"disabled="" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Keterangan</label>
                                            <div>
                                                <input type="text" class="form-control" id="jumlah" name="jumlah" value="{{ $retur->keterangan }}"disabled="" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Date Created</label>
                                            <div>
                                                <input type="text" class="form-control" id="created_at" name="created_at" value="{{ $retur->created_at }}" disabled="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Last Modifield</label>
                                            <div>
                                                <input type="text" class="form-control" id="updated_at" name="updated_at" value="{{ $retur->updated_at }}" disabled="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="#"  data-toggle="modal" data-target="#lihat-modal-{{ $retur->id  }}" class="btn btn-primary glyphicon glyphicon-eye-open" title="View"></a>
                    </td>
                </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection