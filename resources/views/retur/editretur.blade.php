@extends('layout.default')

@section('title')
    Master Retur - Add
@endsection

@section('content')
    <div class="title_right">
        <h3><a href="/retur" class="btn-index btn btn-primary pull-right col-sm-2 col-sm-10" title="Back"><i class="fa fa-arrow-left"></i></a></h3>
    </div>
    <div class="title_left">
        <h3>Master Retur - Create</h3>
    </div>
    <div class="x_panel">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="/retur">Retur</a></li>
            <li class="breadcrumb-item active"><a href="">Edit</a></li>
        </ol>
        @if(session('alert'))
            <div class="alert {{ session('alert') == 'success' ? 'alert-success' : 'alert-danger' }}">
                <span>@if(session('alert') == 'success') Retur Berhasil Diedit. @else Retur Gagal Diedit. @endif</span>
            </div>
        @endif
        <div class="x_content">
            <form method="post" action="{{route ('retur.update', $returs->id)}}" class="form-horizontal form-label-left" role="form">
                {{method_field('PUT')}}
                @if(/** @var \Illuminate\Support\MessageBag $errors */ $errors->isNotEmpty())
                    <div class="alert alert-warning" role="alert">
                        @foreach($errors->all() as $error)
                            <span>{{ $error }}</span>
                            <br>
                        @endforeach
                    </div>
                @endif
                {{csrf_field()}}
                <div class="form-group">
                    <label class="control-label col-sm-3 col-xs-12">Nomor Transaksi</label>
                    <div class="col-sm-3 col-sm-9">
                        <input type="text" id="id_transaksi" name="id_transaksi" value="{{ $returs->id_transaksi }}"required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">
                        @if ($errors->has('id_transaksi'))
                            <span class="help-block">
                              <strong>{{ $errors->first('id_transaksi') }}</strong>
                          </span>
                        @endif
                    </div>

                    {{--  <select class="form-control" name="id_transaksi" id="id_transaksi">
                       @foreach($transaksis as $transaksi)
                       <option value="">-</option>
                       <option value="{{ $transaksi->id }}">{{ $transaksi->id_transaksi }}</option>
                       @endforeach
                     </select> --}}

                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3 col-xs-12">Id barang</label>
                    <div class="col-sm-3 col-sm-9">
                        <select class="form-control" name="id_barang" id="id_barang">
                            @foreach($barangs as $barang)
                                <option value="{{ $barang->id }}">{{ $barang->nama_barang }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3 col-xs-12">Jumlah</label>
                    <div class="col-sm-3 col-sm-9">
                        <input type="text" id="jumlah" name="jumlah" value="{{ $returs->jumlah }}"required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">
                        @if ($errors->has('jumlah'))
                            <span class="help-block">
                              <strong>{{ $errors->first('jumlah') }}</strong>
                          </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3 col-xs-12">Keterangan</label>
                    <div class="col-sm-7 col-xs-12">
                        <textarea id="keterangan" name="keterangan" class="form-control col-md-7 col-xs-12" rows="5">{{ $returs->keterangan }}</textarea>
                        @if ($errors->has('keterangan'))
                            <span class="help-block">
                            <strong>{{ $errors->first('keterangan') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <button class="btn btn-warning" type="/retur">Cancel</button>
                <button class="btn btn-primary" type="submit">Update</button>
            </form>
        </div>
    </div>
@endsection