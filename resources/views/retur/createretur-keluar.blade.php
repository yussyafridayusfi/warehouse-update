@extends('layout.default')

@section('title')
    Master Retur - Add
@endsection

@section('content')
    <div class="title_right">
        <h3><a href="{{route('retur-keluar.index')}}" class="btn-index btn btn-primary pull-right col-sm-2 col-sm-10" title="Back"><i class="fa fa-arrow-left"></i></a></h3>
    </div>
    <div class="title_left">
        <h3>Master Retur - Add</h3>
    </div>
    <div class="x_panel">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Master Data</a></li>
            <li class="breadcrumb-item"><a href="#">Retur</a></li>
            <li class="breadcrumb-item active"><a href="#">Add</a></li>
        </ol>
        @if(session('alert'))
            <div class="alert {{ session('alert') == 'success' ? 'alert-success' : 'alert-danger' }}">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>@if(session('alert') == 'success') Retur Berhasil Ditambahkan. @else Retur Gagal Ditambahkan. @endif</span>
            </div>
        @endif
        <div class="x_content">
          <form method="post" action="{{route ('retur-keluar.store')}}" class="form-horizontal form-label-left" role="form">
              @if(/** @var \Illuminate\Support\MessageBag $errors */ $errors->isNotEmpty())
                  <div class="alert alert-warning" role="alert">
                      @foreach($errors->all() as $error)
                          <span>{{ $error }}</span>
                          <br>
                      @endforeach
                  </div>
              @endif
                {{csrf_field()}}
                
                <div class="form-group">
                  <label class="control-label col-sm-3 col-xs-12">Nomor Transaksi</label>
                  <div class="col-sm-3 col-sm-9">
                    <select class="form-control" name="id_transaksi" id="id_transaksi">
                      <option value="">-Nomor Transaksi-</option>
                      @foreach($transaksis as $transaksi) 
                      <option value="{{ $transaksi->id }}">{{ $transaksi->nomortransaksi }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-sm-3 col-xs-12">Id Barang</label>
                  <div class="col-sm-3 col-sm-9">
                    <input type="text" id="id_barang" name="id_barang" required="required" title="id barang" class="form-control col-md-7 col-xs-12" value="" autofocus="" readonly="">
                        @if ($errors->has('id_barang'))
                          <span class="help-block">
                              <strong>{{ $errors->first('id_barang') }}</strong>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-sm-3 col-xs-12">Id Customer</label>
                  <div class="col-sm-3 col-xs-12">
                      <input type="text" title="" id="id_customer" name="id_customer" value="" required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">
                      @if ($errors->has('id_customer'))
                          <span class="help-block">
                              <strong>{{ $errors->first('id_customer') }}</strong>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-sm-3 col-xs-12">Jumlah</label>
                  <div class="col-sm-3 col-xs-12">
                      <input type="number" pattern="[0-9]+" title="nilai max" id="jumlah" name="jumlah" value="" required="required" class="form-control col-md-7 col-xs-12" value="" autofocus="">
                      @if ($errors->has('jumlah'))
                          <span class="help-block">
                              <strong>{{ $errors->first('jumlah') }}</strong>
                          </span>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-sm-3 col-xs-12">Keterangan</label>
                  <div class="col-sm-5 col-xs-12">
                      <textarea id="keterangan" name="keterangan" class="form-control col-md-7 col-xs-12" rows="5"></textarea>
                      @if ($errors->has('keterangan'))
                        <span class="help-block">
                            <strong>{{ $errors->first('keterangan') }}</strong>
                        </span>
                    @endif
                  </div>
              </div>
                <button class="btn btn-primary" type="submit">Add</button>
            </form>
        </div>
    </div>
@endsection

@section('script')
      <script type="text/javascript">
        $('#id_transaksi').on('change',e => {
              $.ajax({
                type:'get',
                url:'{!!URL::to('CariBarang')!!}',
                data:{'id':e.target.value},
                dataType:'json',
                success:function(data){
                  $('#id_barang').val(data)
                }
              });
          })
      </script>

      <script type="text/javascript">
        $('#id_transaksi').on('change',e => {
              $.ajax({
                type:'get',
                url:'{!!URL::to('CariCustRetur')!!}',
                data:{'id':e.target.value},
                dataType:'json',
                success:function(data){
                  $('#id_customer').val(data)
                }
              });
          })
      </script>

      <script type="text/javascript">
        $('#id_transaksi').on('change',e => {
              $.ajax({
                type:'get',
                url:'{!!URL::to('CariJumlahRetur')!!}',
                data:{'id':e.target.value},
                dataType:'json',
                success:function(data){
                  $('#jumlah').val(data)
                }
              });
          })
      </script>

      <script type="text/javascript">
        $('#id_transaksi').select2();
      </script>

      
@endsection