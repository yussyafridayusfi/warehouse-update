<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>NKY-GUDANG | Login </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="../vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../css/custom.min.css" rel="stylesheet">
</head>

<body class="login">
<div>
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                <form method="POST" action="{{route ('login')}}">
                {{csrf_field()}}
                
                    <h1>Login Form</h1>
                    @if (session('info'))
                        <div class="alert alert-info">
                            <p>{{ session('info') }}</p>
                        </div>
                    @endif
                    <div>
                        <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama" required="" />
                    </div>
                    <div>
                        <input type="password"id="password" name="password" class="form-control" placeholder="Password" required="" />
                    </div>
                    <div>
                        <button class="btn btn-default" type="submit" >Log in</button>
                        
                    </div>

                    <div class="clearfix"></div>

                    <div class="separator">
                        <br />
                        <div>
                            <h1><i class="fa fa-home"></i> NKY-GUDANG </h1>
                            {{-- <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p> --}}
                            <p>©2018 All Rights Reserved. NKY-GUDANG. Privacy and Terms [Copyright Gentelella]</p>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
</div>
</body>
</html>
