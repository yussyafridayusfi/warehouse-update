<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/icon', function () {
    return view('icon');
});

//web profile

Route::get('/webProfile','WebProfileController@getHome')->name('webProfile');
Route::get('/webProfileTentang','WebProfileController@getProfile')->name('tentang');
Route::get('/webProfileLaptop','WebProfileController@getLaptop')->name('laptop');
Route::get('/webProfileGaming','WebProfileController@getGaming')->name('gaming');
Route::get('/webProfileNotebook','WebProfileController@getNotebook')->name('notebook');
Route::get('/webProfileNetbook','WebProfileController@getNetbook')->name('netbook');
Route::get('/webProfileMacbook','WebProfileController@getMacbook')->name('macbook');


//dynamic
//Route::get('/webProfile/{category}','webProfileController@category');


//redirect back
// Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
// Route::post('login', 'Auth\LoginController@login');

//fix with true false auth
Route::get('login','authController@getLogin')->name('login');
Route::post('login','authController@postLogin')->name('login');


//redirect in /home
// Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
// Route::post('register', 'Auth\RegisterController@register');

//register/create user
Route::get('register','authController@getRegister')->name('register');
Route::post('register','authController@postRegister');


	Route::group(['prefix' => '/dashboard', 'as' => 'dashboard.'], function () {
		Route::get('/data', 'dashboardController@index')->name('dashboard');
		Route::get('/chart', 'chartController@index')->name('chartbar');
	});

	//BARANG
	Route::group(['prefix' => '/barang', 'as' => 'barang.'], function () {
		Route::get('/data','barangController@index')->name('index');
		Route::get('/create','barangController@create')->name('create');
		Route::post('/store','barangController@store')->name('store');
		Route::get('/{id}/edit','barangController@edit')->name('edit');
		Route::get('/{id}/show','barangController@show')->name('show');
		Route::put('/{id}/update','barangController@update')->name('update');
		Route::delete('/{id}/destroy','barangController@destroy')->name('destroy');
	});

	//KATEGORI
	Route::group(['prefix' => '/kategori', 'as' => 'kategori.'], function () {
		Route::get('/data','kategoriController@index')->name('index');
		Route::get('/create','kategoriController@create')->name('create');
		Route::post('/store','kategoriController@store')->name('store');
		Route::get('/{id}/edit','kategoriController@edit')->name('edit');
		Route::get('/{id}/show','kategoriController@show')->name('show');
		Route::put('/{id}/update','kategoriController@update')->name('update');
		Route::delete('/{id}/destroy','kategoriController@destroy')->name('destroy');
	});

	//SUPPLIER
	Route::group(['prefix' => '/supplier', 'as' => 'supplier.'], function () {
		Route::get('/data','supplierController@index')->name('index');
		Route::get('/create','supplierController@create')->name('create');
		Route::post('/store','supplierController@store')->name('store');
		Route::get('/{id}/edit','supplierController@edit')->name('edit');
		Route::get('/{id}/show','supplierController@show')->name('show');
		Route::put('/{id}/update','supplierController@update')->name('update');
		Route::delete('/{id}/destroy','supplierController@destroy')->name('destroy');
	});

	//CUSTOMER
	Route::group(['prefix' => '/customer', 'as' => 'customer.'], function () {
		Route::get('/data','customerController@index')->name('index');
		Route::get('/create','customerController@create')->name('create');
		Route::post('/store','customerController@store')->name('store');
		Route::get('/{id}/edit','customerController@edit')->name('edit');
		Route::get('/{id}/show','customerController@show')->name('show');
		Route::put('/{id}/update','customerController@update')->name('update');
		Route::delete('/{id}/destroy','customerController@destroy')->name('destroy');
	});

	//USER
	Route::group(['prefix' => '/user', 'as' => 'user.'], function () {
		Route::get('/data','userController@index')->name('index');
		Route::get('/create','userController@create')->name('create');
		Route::post('/store','userController@store')->name('store');
		Route::get('/{id}/edit','userController@edit')->name('edit');
		Route::get('/{id}/show','userController@show')->name('show');
		Route::put('/{id}/update','userController@update')->name('update');
		Route::delete('/{id}/destroy','userController@destroy')->name('destroy');
	});

	//TRANSAKSI-MASUK
	Route::group(['prefix' => '/transaksi-masuk', 'as' => 'transaksi-masuk.'], function () {
		Route::get('/data','transaksiController@index')->name('index');
		Route::get('/create','transaksiController@create')->name('create');
		Route::post('/store','transaksiController@store')->name('store');
		Route::get('/{id}/show','transaksiController@show')->name('show');
	});

	Route::get('/CariHarga','transaksiController@CariHarga');
	Route::get('/CariJumlah','transaksiController@CariJumlah');

	//TRANSAKSI-KELUAR
	Route::group(['prefix' => '/transaksi-keluar', 'as' => 'transaksi-keluar.'], function () {
		Route::get('/data','transaksikeluarController@index')->name('index');
		Route::get('/create','transaksikeluarController@create')->name('create');
		Route::post('/store','transaksikeluarController@store')->name('store');
		Route::get('/{id}/show','transaksikeluarController@show')->name('show');
	});

	Route::get('/CariHargaK','transaksikeluarController@CariHargaK');
	Route::get('/CariJumlahK','transaksikeluarController@CariJumlahK');


	//RETUR-NEW
	//RETUR-KELUAR
	Route::group(['prefix' => '/retur-keluar', 'as' => 'retur-keluar.'], function () {
		Route::get('/data','returController@index')->name('index');
		Route::get('/create','returController@create')->name('create');
		Route::post('/store','returController@store')->name('store');
		Route::get('/{id}/show','returController@show')->name('show');
	});

	Route::get('/CariBarang','returController@CariBarang');
	Route::get('/CariJumlahRetur','returController@CariJumlahRetur');
	Route::get('/CariCustRetur','returController@CariCustRetur');


	//TRANSAKSI-MASUK
	Route::group(['prefix' => '/retur-masuk', 'as' => 'retur-masuk.'], function () {
		Route::get('/data','returMasukController@index')->name('index');
		Route::get('/create','returMasukController@create')->name('create');
		Route::post('/store','returMasukController@store')->name('store');
		Route::get('/{id}/show','returMasukController@show')->name('show');
	});

	Route::get('/CariBarang','returMasukController@CariBarang');
	Route::get('/CariJumlahRetur','returMasukController@CariJumlahRetur');
	Route::get('/CariSupRetur','returMasukController@CariSupRetur');

	//LAPORAN
	Route::get('/laporan-membership','dashboardController@getLaporanMember');
	Route::post('/getDataMember','dashboardController@getDataMember')->name('getDataMember');

	Route::get('/laporan-barang','dashboardController@getLaporanBarang');
	Route::post('/getDataBarang','dashboardController@getDataBarang')->name('getDataBarang');
	
	Route::get('/laporan-transaksi','dashboardController@getLaporanTrans');
	Route::post('/getDataTrans','dashboardController@getDataTrans')->name('getDataTrans');

	Route::get('/laporan-retur','dashboardController@getLaporanRetur');
	Route::post('/getDataRetur','dashboardController@getDataRetur')->name('getDataRetur');
	
	//LOGS
	Route::get('/logs-masuk','LogsController@getLapMasuk');
	Route::post('/getDataLapKeluar','LogsController@getDataLapKeluar')->name('getDataLapKeluar');

	Route::get('/logs-keluar','LogsController@getLapKeluar');
	Route::post('/getDataLapMasuk','LogsController@getDataLapMasuk')->name('getDataLapMasuk');

	Route::get('/logs-retur-masuk','LogsController@getLapRMasuk');
	Route::post('/getDataLapRMasuk','LogsController@getDataLapRMasuk')->name('getDataLapRMasuk');

	Route::get('/logs-retur-keluar','LogsController@getLapRKeluar');
	Route::post('/getDataLapRKeluar','LogsController@getDataLapRKeluar')->name('getDataLapRKeluar');

	Route::get('logout', 'Auth\LoginController@logout');

	Route::get('/datatransaksi','transaksiKeluarController@JenisBarang');
	Route::get('/datauang','transaksiKeluarController@uang');


//dashboard
// Route::get('dashboard', 'dashboardController@index')->name('dashboard');


//route resource for crud action
// Route::resource('user','userController')->middleware('auth');
// Route::resource('customer','customerController')->middleware('auth');
// Route::resource('supplier','supplierController')->middleware('auth');
// Route::resource('kategori','kategoriController')->middleware('auth');
// Route::resource('barang','barangController')->middleware('auth');
// Route::resource('transaksi','transaksiController')->middleware('auth');
// Route::get('dashboard', 'dashboardController@index')->middleware('auth')->name('dashboard');
// Route::resource('retur','returController')->middleware('auth');

// Route::get('logout', 'Auth\LoginController@logout')->name('logout');


//grouping middleware
// Route::group(['middleware' => 'auth'],function(){
	// Route::get('dashboard', 'dashboardController@index')->name('dashboard');

	// Route::get('dashboard/chart', 'chartController@index')->name('chartbar');
	
	// Route::resource('user','userController');
	
	// Route::resource('customer','customerController');

	// Route::resource('supplier','supplierController');

	// Route::resource('kategori','kategoriController');

	// Route::resource('barang','barangController');


	//old
	// Route::get('transaksi-masuk','transaksiController@index');
	// Route::get('transaksi-masuk/create','transaksiController@create');
	// Route::get('transaksi-masuk/store','transaksiController@store')->name('transaksi-masuk.store');
	// Route::get('transaksi-masuk/show','transaksiController@show')->name('transaksi-masuk.show');
	//Route::get('/getDataTransMasuk','transaksiController@getDataTransMasuk')->name('getDataTransMasuk');

	// Route::get('transaksi-keluar','transaksikeluarController@index');
	// Route::get('transaksi-keluar/create','transaksikeluarController@create');
	// Route::get('transaksi-keluar/store','transaksikeluarController@store')->name('transaksi-keluar.store');
	// Route::get('transaksi-keluar/show','transaksikeluarController@show')->name('transaksi-keluar.show');

	//RETUR
	// Route::get('retur-keluar','returController@index');
	// Route::get('retur-keluar/create','returController@create');
	// Route::post('retur-keluar/store','returController@store')->name('retur-keluar.store');
	// Route::get('retur-keluar/show','returController@show')->name('retur-keluar.show');

	// Route::get('retur-masuk','returMasukController@index');
	// Route::get('retur-masuk/create','returMasukController@create');
	// Route::post('retur-masuk/store','returMasukController@store')->name('retur-masuk.store');
	// Route::get('retur-masuk/show','returMasukController@show')->name('retur-masuk.show');
	


// });



// Route::get('user','userController@index');
	// Route::get('user/create','userController@create');
	// Route::get('user/store','userController@store')->name('user.store');
	// Route::get('user/edit','userController@edit')->name('user.edit');
	// Route::get('user/show','userController@show')->name('user.show');



// //route without login authtentication
// Route::get('/login','authController@getLogin');
// Route::post('/login','authController@postLogin')->name('login');
// Route::get('/home',function(){
//     //return auth()->user();
//    return view('dashboard');
// })->name('home');
// Route::get('/logout','authController@logout')->name('logout');


// Route::get('/register','authController@getRegister')->name('register');
// Route::post('/register','authController@postRegister');


// // Route::post('/register','authController@postRegister');
// Route::post('/register','RegisterController');
// Route::get('/masteruser','profileController@show')->name('masteruser');
// Route::get('/createcustomer','customerController@getCustomer')->name('createcustomer');
// Route::get('/mastercustomer','customerController@show')->name('mastercustomer');


// Route::get('/customer', function () {
//     return view('/customer');
// });
// Route::resource('customer', 'customerController');




//Routes with login authentication
// Route::get('/login','authController@getLogin')->middleware('guest');
// Route::post('/login','authController@postLogin')->middleware('guest')->name('login');
// Route::get('/dashboard',function(){
//     //return auth()->user();
//    return view('dashboard');
// })->middleware('auth')->name('home');

// Route::get('/logout','authController@logout')->middleware('auth')->name('logout');

// Route::get('/editProfile','authController@editProfile')->middleware('auth')->name('edit');
//Route::resource('layout','profileController');
// Route::get('/masterUser','profileController@show')->middleware('auth')->name('masteruser');
// // Route::get('/masterUserLevel','profileController@showMasterUserLevel')->middleware('auth')->name('masteruserlevel');
 // Route::get('/register','authController@getRegister')->middleware('auth')->name('register');
 
